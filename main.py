import mysql.connector
from tkinter import *
from tkinter import messagebox
from functools import partial;
import tkinter
import tkinter as tk

import tkinter.ttk as ttk


class window:

    execute_sql = True

    window_width = 1000
    window_height = 700

    button_window_height = 33
    button_window_width = 142



    button_width = int(button_window_width / 2)
    button_height = int(button_window_height / 2)

    button_width_2 = int(button_window_width / 3)

    canvas = None

    create_btn = None
    read_btn = None
    update_btn = None
    delete_btn = None

    function = ""
    entity = ""

    '''
    create_clicked = False
    read_clicked = False
    update_clicked = False
    delete_clicked = False
    '''

    order_btn = None
    dining_hall_btn = None
    dining_plan_btn = None
    employee_btn = None
    food_item_btn = None
    forum_btn = None
    restaurant_btn = None
    student_btn = None
    all_item_btn = None

    # For Home Page
    emp_btn = None
    stu_btn = None
    ad_btn= None
    welcome_label_text = None
    welcome_label = None

    order_clicked = False
    dining_hall_clicked = False
    dining_plan_clicked = False
    employee_clicked = False
    food_item_clicked = False
    forum_clicked = False
    restaurant_clicked = False
    student_clicked = False
#
    dining_hall_name = None
    hours_box = None
    location_box = None

    mycursor = None

    mydb = None
    usertype = None

    first_name = None
    last_name = None
    food_name = None
    restaurant = None

    food_item_index = 21
    order_index = 21
    forum_index = 21
    restaurant_index = 21
    dining_hall_index = 21
    employee_index = 21

    first_name_label = None
    last_name_label = None
    food_name_label = None
    restaurant_label = None

    insert_btn = None

    forum_content = None
    stars_content = None

    admin_create_food_btn = None
    admin_create_dining_hall_btn = None
    admin_create_restaurant_btn = None
    # for the read displays
    all_text = None
    nextt_text = None
    nextt_text_btn = None
    user_input__label = None
    user_input_text_label = None
    food_info_text = None
    food_info_label = None
    food_item_name = None
    f_text = None
    f_text_label = None
    ff_text = None
    ff_text_label = None

    user_input_dh_text_label = None
    user_input_dh_label = None
    #blah
    dh_item_name = None
    nextt_dh_text = None
    nextt_dh_text_btn = None
    dh_info_text = None
    dh_info_label = None
    dh_text = None
    dh_text_label = None
    dhh_text = None
    dhh_text_label = None

    user_input_r_text_label = None
    user_input_r_label = None
    r_item_name = None
    nextt_r_text_btn = None
    nextt_r_text = None
    r_text = None
    r_text_label = None
    r_info_text = None
    r_info_text = None

    user_input_s_text_label = None
    user_input_s_label = None
    s_item_name = None
    user_input_ss_text_label = None
    user_input_ss_label = None
    ss_item_name = None
    user_input_sss_text_label = None
    user_input_sss_label = None
    sss_item_name = None
    nextt_s_text = None
    nextt_s_text_btn = None
    s_text = None
    s_text_label = None
    s_info_text = None
    s_info_text = None

    user_input_e_text_label = None
    user_input_e_label = None
    e_item_name = None
    user_input_ee_text_label = None
    user_input_ee_label = None
    ee_item_name = None
    user_input_eee_text_label = None
    user_input_eee_label = None
    eee_item_name = None
    nextt_e_text = None
    nextt_e_text_btn = None
    e_text = None
    e_text_label = None
    e_info_text = None
    e_info_text = None



    carbs = None
    fat = None
    protein = None
    calories = None

    hall_name = None
    hall_hours = None
    hall_location = None

    d_hall = None
    hours = None
    restaurant_name = None

    forum_label = None

    back_btn = None
    logout_btn = None


    protein_label = None
    calories_label = None
    admin_insert_btn = None
    carbs_label = None
    fat_label = None
    calories_label = None
    protein_label = None

    restaurant_name_label = None
    hours_label = None
    res_dining_hall_label = None
    admin_insert_btn = None

    hall_label = None
    hall_hours_label = None
    hall_location_label = None
    admin_insert_btn = None

    register_frame = None
    input_frame = None
    forgot_frame = None
    nm_text_label = None

    register_button = None

    input_frame = None

    forgot_pass = None

    all_text_label = None

    rr_text_label = None

    days_worked_text_label = None
    days_worked_label = None
    days_worked_text = None

    hours_worked_text_label = None
    hours_worked_label = None
    hours_worked_text = None

    location_worked_text_label = None
    location_worked_label = None
    location_worked_text = None

    user_input_m_text_label = None
    user_input_m_label = None
    m_item_name = None

    user_input_mm_text_label = None
    user_input_mm_label = None
    mm_item_name = None

    user_input_mmm_text_label = None
    user_input_mmm_label = None
    mmm_item_name = None

    nextt_m_text = None
    nextt_m_text_btn = None

    view_btn = None
    admin_create_admin_btn = None

    user_label = None

    stat_label = None

    e_info_label = None
    r_info_label = None
    s_info_label = None

    food_btn = None
    view_text = None


    def toggle(self):
        if self.password_checkbutton.var.get():
            self.password_txt.config(show="*")
        else:
            self.password_txt.config(show="")

    def ftoggle(self):
        if self.fpassword_checkbutton.var.get():
            self.fpassword_txt.config(show="*")
            self.cfpassword_txt.config(show="*")
        else:
            self.fpassword_txt.config(show="")
            self.cfpassword_txt.config(show="")
    def __init__(self, master):

        self.mydb = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
        self.mycursor = mydb.cursor()

        self.master = master

        self.canvas = tk.Canvas(master, width=self.window_width, height=self.window_height)
        self.canvas.grid(columnspan=3)



        self.loginform()


    #Sets up login page
    def login(self):
        print("being called")
        if self.username_txt.get()=="" or self.password_txt.get()=="":
            messagebox.showerror("Error", "Please fill out all the fields", parent=self.master)
        else:
            mycursor.execute('SELECT * FROM foodapp.DB_USER WHERE Username=%s and UserPassword=%s', (self.username_txt.get(), self.password_txt.get()))
            row = mycursor.fetchone()
            if row == None:
                messagebox.showerror("Error", "Invalid Username and Password", parent=self.master)
                self.loginclear()
                self.username_txt.focus()
            else:
                # Take to User specific page
                if row[4] == '1':
                    self.usertype = "admin"
                    if self.register_frame is not None:
                        self.clearRegisterPage()
                    if self.forgot_frame is not None:
                        self.clearChangePage()
                    self.clearLoginPage()
                    self.function_page(self.master, self.usertype)
                elif row[4] == '2':

                    if self.register_frame is not None:
                        self.clearRegisterPage()
                    if self.forgot_frame is not None:
                        self.clearChangePage()
                    self.clearLoginPage()
                    self.usertype = "employee"
                    self.function_page(self.master, self.usertype)
                else:
                    if self.register_frame is not None:
                        self.clearRegisterPage()
                    if self.forgot_frame is not None:
                        self.clearChangePage()
                    self.clearLoginPage()
                    self.usertype = "student"
                    self.function_page(self.master, self.usertype)

    def clearLoginPage(self):
        self.input_frame.destroy()
        self.username_label.destroy()
        self.username_txt.destroy()
        self.password_label.destroy()
        self.password_txt.destroy()
        self.password_checkbutton.destroy()
        self.forgot_pass.destroy()
        self.login_button.destroy()
        self.register_button.destroy()
        self.welcome_label.destroy()

    def clearRegisterPage(self):
        self.register_frame.destroy()
        self.register_label.destroy()
        self.rusername_label.destroy()
        self.rusername_txt.destroy()
        self.email_label.destroy()
        self.email_txt.destroy()
        self.rpassword_label.destroy()
        self.rpassword_txt.destroy()
        self.confirm_rpassword_label.destroy()
        self.crpassword_txt.destroy()
        self.studentCheck.destroy()
        self.studentselectLabel.destroy()
        self.employeeCheck.destroy()
        self.employeeselectLabel.destroy()
        self.register_btn.destroy()
        self.login_btn.destroy()
    def clearChangePage(self):
        self.forgot_frame.destroy()
        self.forgot_label.destroy()
        self.fusername_label.destroy()
        self.fusername_txt.destroy()
        self.femail_label.destroy()
        self.femail_txt.destroy()
        self.fpassword_label.destroy()
        self.fpassword_txt.destroy()
        self.fconfirm_password_label.destroy()
        self.cfpassword_txt.destroy()
        self.fpassword_checkbutton.destroy()
        self.confirm_btn.destroy()
        self.goBack_btn.destroy()


    def register(self):
        if (self.rusername_txt.get()=="" or self.email_txt.get()=="" or self.rpassword_txt.get()==""or
            self.crpassword_txt.get()==""):
            messagebox.showerror("Error", "All fields are required to be filled out", parent=self.master)
        elif self.rpassword_txt.get() != self.crpassword_txt.get():
            messagebox.showerror("Error", "Passwords do not match. Please confirm your password correctly.",
                                 parent=self.master)
        else:
            mycursor.execute("SELECT * FROM foodapp.DB_USER WHERE Email=%s", (self.email_txt.get(),))
            row=mycursor.fetchone()
            if row != None:
                messagebox.showerror("Error", "Email already in use", parent=self.master)
                self.regclear()
                self.rusername_txt.focus()
            else:
                mycursor.execute('SELECT * FROM foodapp.DB_USER WHERE Username=%s', (self.rusername_txt.get(),))
                row=mycursor.fetchone()
                if row != None:
                    messagebox.showerror("Error", "Username already in use", parent=self.master)
                    self.regclear()
                    self.rusername_txt.focus()
                else:
                    mycursor.execute('INSERT INTO foodapp.DB_USER (Username, Email, UserPassword, UserType) VALUES(%s,%s,%s,%s)',
                                     (self.rusername_txt.get(), self.email_txt.get(), self.rpassword_txt.get(),
                                      self.userVar.get()))
                    mydb.commit()
                    messagebox.showinfo("Success", "Register Successful", parent=self.master)
                    self.regclear()
                    self.rusername_txt.focus()

    def regclear(self):
        self.rusername_txt.delete(0, END)
        self.email_txt.delete(0,END)
        self.rpassword_txt.delete(0,END)
        self.crpassword_txt.delete(0,END)



    def registerForm(self):
        if self.input_frame is not None:
            self.clearLoginPage()
        if self.forgot_frame is not None:
            self.clearChangePage()
        self.register_frame = Frame(self.master, bg="#861F41")
        self.register_frame.place(x=10, y=50, height=400, width=300)

        self.register_label = Label(self.register_frame, text="Register Here", font=('Times New Roman', 26), fg="white", bg="#861F41")
        self.register_label.place(x=150, y=20, anchor="center")

        self.rusername_label = Label(self.register_frame, text="Username", font=('Times New Roman', 12), fg="white", bg="#861F41")
        self.rusername_label.place(x=10, y=50)
        self.rusername_txt = Entry(self.register_frame, font=("Times New Roman", 10, "bold"))
        self.rusername_txt.place(x=80, y=50, width=145)

        self.email_label = Label(self.register_frame, text="Email", font=('Times New Roman', 12), fg= "white", bg="#861F41")
        self.email_label.place(x=10, y=80)
        self.email_txt = Entry(self.register_frame, font=("Times New Roman", 10, "bold"))
        self.email_txt.place(x=80, y=80, width=145)

        self.rpassword_label = Label(self.register_frame, text="Password", font=('Times New Roman', 12), fg="white", bg="#861F41")
        self.rpassword_label.place(x=10, y=110)
        self.rpassword_txt = Entry(self.register_frame, font=("Times New Roman", 10, "bold"), show="*")
        self.rpassword_txt.place(x=80, y=110, width=145)

        self.confirm_rpassword_label = Label(self.register_frame, text="Confirm\nPassword", font=('Times New Roman', 12), fg="white", bg="#861F41")
        self.confirm_rpassword_label.place(x=10, y=130)
        self.crpassword_txt = Entry(self.register_frame, font=("Times New Roman", 10, "bold"), show="*")
        self.crpassword_txt.place(x=80, y=140, width=145)


        self.userVar = IntVar()
        self.studentCheck = Radiobutton(self.register_frame, variable=self.userVar, value=3, fg="black", bg="#861F41",
                                        activebackground="#861F41")
        self.studentCheck.place(x=80, y=170)
        self.studentselectLabel = Label(self.register_frame, text="Student", font=('Times New Roman', 12), fg="white",
                                   bg="#861F41")
        self.studentselectLabel.place(x=100, y=170)
        self.employeeCheck = Radiobutton(self.register_frame, variable=self.userVar, value=2, fg="black", bg="#861F41",
                                        activebackground="#861F41")
        self.employeeCheck.place(x=160, y=170)
        self.employeeselectLabel = Label(self.register_frame, text="Employee", font=('Times New Roman', 12), fg="white",
                                   bg="#861F41")
        self.employeeselectLabel.place(x=180, y=170)

        self.register_btn = Button(self.register_frame, command=self.register, text="Register", cursor="hand2", font=("Times New Roman", 10),
                              fg="white", bg="#861F41")
        self.register_btn.place(x=150, y= 220, anchor="center", height=20)

        self.login_btn = Button(self.register_frame, command=self.loginform, text="Already Registered? Login", cursor="hand2", font=("Times New Roman", 12),
                              fg="white", bg="#861F41", bd=0, activebackground="#861F41")
        self.login_btn.place(x=150, y= 260, anchor="center")



    def loginclear(self):
        self.username_txt.delete(0,END)
        self.password_txt.delete(0,END)

    def loginform(self):

        if self.register_frame is not None:
            self.clearRegisterPage()
        if self.forgot_frame is not None:
            self.clearChangePage()

        welcome_label_text = tk.StringVar()
        self.welcome_label = tk.Label(self.master, textvariable=welcome_label_text, pady=10, font=("Times New Roman", 24))
        self.welcome_label.place(x=0, y=0)
        welcome_label_text.set("Welcome to MyVTMealLog")

        self.input_frame = Frame(self.master, bg="#861F41")
        self.input_frame.place(x=10, y=50, height=400, width=300)

        self.login_label = Label(self.input_frame, text="Login Here", font=('Times New Roman', 26), fg="white", bg="#861F41")
        self.login_label.place(x=150, y=20, anchor="center")

        #Username section of Login Page
        self.username_label = Label(self.input_frame, text="Username",font=('Times New Roman', 12), fg="white", bg="#861F41")
        self.username_label.place(x=10, y=50)
        self.username_txt = Entry(self.input_frame, font=("Times New Roman", 10, "bold"))
        self.username_txt.place(x=80, y=50, width = 145)

        #Password Section of Login Page
        self.password_label = Label(self.input_frame, text="Password",font=('Times New Roman',12), fg="white", bg="#861F41")
        self.password_label.place(x=10, y=100)
        self.password_txt = Entry(self.input_frame, font=('Times New Roman', 10), show='*')
        self.password_txt.place(x=80, y=100, width = 145)
        self.password_checkbutton = tk.Checkbutton(self.master, text="Show Password", onvalue=False, offvalue=True, command=self.toggle, fg="black",
                                                   bg="#861F41", activebackground="#861F41")
        self.password_checkbutton.var = tk.BooleanVar(value=True)
        self.password_checkbutton['variable'] = self.password_checkbutton.var
        self.password_checkbutton.place(x=100, y=170)

        self.forgot_pass = Button(self.input_frame, text="Forgot Password?", command=self.forgotForm, cursor="hand2", font=('Times New Roman', 10), fg="black", bg="#861F41", bd=0,
                             activebackground="#861F41")
        self.forgot_pass.place(x=150, y= 190, anchor="center")

        self.login_button = Button(self.input_frame, text="Login", command=self.login, cursor="hand2", font=('Times New Roman',10), fg="black",bg="#861F41")
        self.login_button.place(x=150, y=160, anchor="center")

        self.register_button = Button(self.input_frame, text="Not Registered? Register", command=self.registerForm, cursor="hand2",
                                 font=('Times New Roman',10), fg="black", bg="#861F41", bd=0, activebackground="#861F41")
        self.register_button.place(x=150, y=210, anchor="center")

    def forgotForm(self):
        if self.register_frame is not None:
            self.clearRegisterPage()
        if self.input_frame is not None:
            self.clearLoginPage()

        self.forgot_frame = Frame(self.master, bg="#861F41")
        self.forgot_frame.place(x=10, y=50, height=400, width=300)

        self.forgot_label = Label(self.forgot_frame, text="Change Password", font=('Times New Roman', 26), fg="white", bg="#861F41")
        self.forgot_label.place(x=150, y=20, anchor="center")

        self.fusername_label = Label(self.forgot_frame, text="Username", font=('Times New Roman', 8), fg="white", bg="#861F41")
        self.fusername_label.place(x=10, y=50)
        self.fusername_txt = Entry(self.forgot_frame, font=("Times New Roman", 10, "bold"))
        self.fusername_txt.place(x=80, y=50, width=145)

        self.femail_label = Label(self.forgot_frame, text="Email", font=('Times New Roman', 8), fg="white", bg="#861F41")
        self.femail_label.place(x=10, y=80)
        self.femail_txt = Entry(self.forgot_frame, font=("Times New Roman", 10, "bold"))
        self.femail_txt.place(x=80, y=80, width=145)

        self.fpassword_label = Label(self.forgot_frame, text="New Password", font=('Times New Roman', 8), fg="white", bg="#861F41")
        self.fpassword_label.place(x=10, y=110)
        self.fpassword_txt = Entry(self.forgot_frame, font=('Times New Roman', 10, "bold"), show='*')
        self.fpassword_txt.place(x=80, y=110, width=145)

        self.fconfirm_password_label = Label(self.forgot_frame, text="Confirm\nPassword", font=('Times New Roman', 8),
                                        fg="white", bg="#861F41")
        self.fconfirm_password_label.place(x=10, y=130)
        self.cfpassword_txt = Entry(self.forgot_frame, font=("Times New Roman", 10, "bold"), show="*")
        self.cfpassword_txt.place(x=80, y=140, width=145)

        self.fpassword_checkbutton = tk.Checkbutton(self.master, text="Show Password", onvalue=False, offvalue=True,
                                                    command=self.ftoggle, fg="black",
                                                    bg="#861F41", activebackground="#861F41")
        self.fpassword_checkbutton.var = tk.BooleanVar(value=True)
        self.fpassword_checkbutton['variable'] = self.fpassword_checkbutton.var
        self.fpassword_checkbutton.place(x=100, y=220)


        self.confirm_btn = Button(self.forgot_frame, command=self.forgot, text="Done", cursor="hand2",
                              font=("Times New Roman", 10),
                              fg="white", bg="#861F41", bd=0)

        self.confirm_btn.place(x=180, y=240, anchor="center", height=20)

        self.goBack_btn = Button(self.forgot_frame, command=self.loginform, text="Go Back", cursor="hand2",
                              font=("Times New Roman", 10),
                              fg="white", bg="#861F41", bd=0)
        self.goBack_btn.place(x=120, y=240, anchor="center", height=20)

    def forgot(self):
        if (self.fusername_txt.get()=="" or self.femail_txt.get()=="" or self.fpassword_txt.get()==""or
            self.cfpassword_txt.get()==""):
            messagebox.showerror("Error", "All fields are required to be filled out", parent=self.master)
        elif self.fpassword_txt.get() != self.cfpassword_txt.get():
            messagebox.showerror("Error", "Passwords do not match. Please confirm your password correctly.",
                                 parent=self.master)
        else:
            mycursor.execute("SELECT * FROM foodapp.DB_USER WHERE Email=%s AND Username=%s", (self.femail_txt.get(),
                                                                                              self.fusername_txt.get()))
            row=mycursor.fetchone()
            if row == None:
                messagebox.showerror("Error", "No existing user with that email and username", parent=self.master)
                self.fusername_txt.focus()
            else:
                mycursor.execute('UPDATE foodapp.DB_USER SET UserPassword=%s WHERE Username=%s AND Email=%s',
                                     (self.fpassword_txt.get(),self.fusername_txt.get(), self.femail_txt.get()))
                mydb.commit()
                messagebox.showinfo("Success", "Change Successful", parent=self.master)
                self.fclear()
                self.fusername_txt.focus()
                self.loginform()

    def fclear(self):
        self.fusername_txt.delete(0, END)
        self.femail_txt.delete(0, END)
        self.fpassword_txt.delete(0, END)
        self.cfpassword_txt.delete(0, END)

    def function_page(self, master, clicked):

        print("hello")
        # store the kind of user selected in usertype global variable


        # destroy buttons
        #self.emp_btn.destroy()
        #self.stu_btn.destroy()
        #self.ad_btn.destroy()
        # send it to function page
        print(self.welcome_label == None)
        self.welcome_label.destroy()


        if clicked == "admin":
            print("hi")
            view_text = tk.StringVar()
            self.view_btn = tk.Button(master, textvariable=view_text, height=5,
                                        width=20, command=lambda: self.view_window(self.master))
            self.view_btn.place(x=self.window_width/2 - 80, y=self.window_height/2 - 90)
            view_text.set("View Stats")
            #blahh

        create_text = tk.StringVar()
        if clicked == "admin" or clicked == "student":
            self.create_btn = tk.Button(master, textvariable=create_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.create_window(self.master))
            self.create_btn.place(x=0, y=0)
        elif clicked == "employee":
            self.create_btn = tk.Button(master, textvariable=create_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.create_window_2(self.master))
            self.create_btn.place(x=0, y=0)
        create_text.set("Create")

        read_text = tk.StringVar()
        self.read_btn = tk.Button(master, textvariable=read_text, height=self.button_height,
                                  width=self.button_width, command=lambda: self.admin_read(self.master))
        self.read_btn.place(x=0, y=int(self.window_height / 2))
        read_text.set("Read")

        # update_text = tk.StringVar()
        # self.update_btn = tk.Button(master, textvariable=update_text, height=self.button_height,
        #                             width=self.button_width, command=lambda: self.clear_window("update", self.master))
        # self.update_btn.place(x=int(self.window_width / 2), y=0)
        # update_text.set("Update")

        if clicked == "admin":
            delete_text = tk.StringVar()
            self.delete_btn = tk.Button(master, textvariable=delete_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.clear_window_admin(self.master))
            self.delete_btn.place(x=int(self.window_width / 2), y=int(self.window_height / 2))
            delete_text.set("Delete")

        back_text = tk.StringVar()
        self.back_btn = tk.Button(master, textvariable = back_text, height=4, width =25, command=lambda: self.back_first(self.master))
        self.back_btn.place(x = 20, y = 620)
        back_text.set("Back")

        logout_text = tk.StringVar()
        self.logout_btn = tk.Button(master, textvariable=logout_text,
                                  command=lambda: self.logout(self.master))
        self.logout_btn.place(x=self.window_width - 90, y=640)
        logout_text.set("Logout")


    def logout(self, master):
        print("hello")
        for widgets in master.winfo_children():
            widgets.destroy()
        self.loginform()

    def back_first(self, master):

        if self.admin_insert_btn is not None:
            self.admin_insert_btn.destroy()

        if self.user_input_m_label is not None:
            self.user_input_m_label.destroy()
            self.m_item_name.destroy()
            self.user_input_mm_label.destroy()
            self.mm_item_name.destroy()
            self.user_input_mmm_label.destroy()
            self.mmm_item_name.destroy()
            self.days_worked_label.destroy()
            self.days_worked_text.destroy()
            self.hours_worked_label.destroy()
            self.hours_worked_text.destroy()
            self.location_worked_label.destroy()
            self.location_worked_text.destroy()
            self.nextt_m_text_btn.destroy()

        print("in back first")
        if self.r_text_label is not None:
            self.r_text_label.destroy()

        if self.s_text_label is not None:
            self.s_text_label.destroy()

        if self.dhh_text_label is not None:
            self.dhh_text_label.destroy()
            self.dh_text_label.destroy()

        if self.dh_info_label is not None:
            self.dh_info_label.destroy()

        if self.e_info_label is not None:
            self.e_info_label.destroy()

        if self.all_text_label is not None:
            self.all_text_label.destroy()

        if self.r_info_label is not None:
            self.r_info_label.destroy()
            self.rr_text_label.destroy()

        if self.s_info_label is not None:
            self.s_info_label.destroy()

        if self.f_text_label is not None:
            self.f_text_label.destroy()
            self.ff_text_label.destroy()

        if self.e_text_label is not None:
            self.e_text_label.destroy()

        if self.user_label is not None:
            self.user_label.destroy()
            self.user_name.destroy()
            self.pass_label.destroy()
            self.passw.destroy()
            self.cpass_label.destroy()
            self.cpassw.destroy()
            self.em_label.destroy()
            self.em.destroy()
            self.admin_insert_btn.destroy()

        if self.first_name is not None:
            self.first_name.destroy()
            self.last_name.destroy()
            self.food_name.destroy()
            # self.restaurant.destroy()

            self.first_name_label.destroy()
            self.last_name_label.destroy()
            # self.restaurant_label.destroy()
            self.food_name_label.destroy()

            self.insert_btn.destroy()

        if self.create_btn is not None:
            self.create_btn.destroy()
            if self.delete_btn is not None:
                self.delete_btn.destroy()
            # self.update_btn.destroy()
            self.read_btn.destroy()
            self.back_btn.destroy()

        if self.forum_label is not None:
            self.forum_label.destroy()
            self.forum_content.destroy()
            self.stars_label.destroy()
            self.stars_content.destroy()
            self.insert_btn.destroy()

        if self.admin_create_restaurant_btn is not None:
            self.admin_create_restaurant_btn.destroy()
            self.admin_create_food_btn.destroy()
            self.admin_create_dining_hall_btn.destroy()
            # self.restaurant_label.destroy()
            # self.restaurant.destroy()
            # self.carbs_label.destroy()
            # self.carbs.destroy()
            # self.fat_label.destroy()
            # self.fat.destroy()
            # self.protein_label.destroy()
            # self.protein.destroy()
            # self.calories_label.destroy()
            # self.calories.destroy()
            # self.admin_insert_btn.destroy()

        if self.food_name_label is not None:
            self.food_name_label.destroy()
            self.food_name.destroy()
            if self.restaurant_label is not None:
                self.restaurant_label.destroy()
                self.restaurant.destroy()
                if self.carbs_label is not None:
                    self.carbs_label.destroy()
                    self.carbs.destroy()
                    self.fat_label.destroy()
                    self.fat.destroy()
                    self.protein_label.destroy()
                    self.protein.destroy()
                    self.calories_label.destroy()
                    self.calories.destroy()
                    self.admin_insert_btn.destroy()

        if self.restaurant_name_label is not None:
            self.restaurant_name_label.destroy()
            self.restaurant_name.destroy()
            if self.hours_label is not None:
                self.hours_label.destroy()
                self.hours.destroy()
                self.res_dining_hall_label.destroy()
                self.d_hall.destroy()
                self.admin_insert_btn.destroy()

        if self.hall_label is not None:
            self.hall_label.destroy()
            self.hall_name.destroy()
            if self.hall_hours_label is not None:
                self.hall_hours_label.destroy()
                self.hall_hours.destroy()
                self.hall_location_label.destroy()
                self.hall_location.destroy()
                self.admin_insert_btn.destroy()

        if self.stat_label is not None:
            for widgets in master.winfo_children():
                widgets.destroy()

        # welcome_label_text = tk.StringVar()
        # self.welcome_label = tk.Label(master, textvariable=welcome_label_text, pady=10)
        # self.welcome_label.place(x=10, y=10)
        # welcome_label_text.set("Welcome to MyVTMealLog:")

        if self.usertype == "admin":
            print("hi")
            view_text = tk.StringVar()
            self.view_btn = tk.Button(master, textvariable=view_text, height=5,
                                        width=20, command=lambda: self.view_window(self.master))
            self.view_btn.place(x=self.window_width/2 - 80, y=self.window_height/2 - 90)
            view_text.set("View Stats")

        create_text = tk.StringVar()
        if self.usertype == "admin" or self.usertype == "student":
            self.create_btn = tk.Button(master, textvariable=create_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.create_window(self.master))
            self.create_btn.place(x=0, y=0)
        elif self.usertype == "employee":
            self.create_btn = tk.Button(master, textvariable=create_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.create_window_2(self.master))
            self.create_btn.place(x=0, y=0)
        create_text.set("Create")

        read_text = tk.StringVar()
        self.read_btn = tk.Button(master, textvariable=read_text, height=self.button_height,
                                  width=self.button_width, command=lambda: self.admin_read(self.master))
        self.read_btn.place(x=0, y=int(self.window_height / 2))
        read_text.set("Read")

        # update_text = tk.StringVar()
        # self.update_btn = tk.Button(master, textvariable=update_text, height=self.button_height,
        #                             width=self.button_width, command=lambda: self.clear_window("update", self.master))
        # self.update_btn.place(x=int(self.window_width / 2), y=0)
        # update_text.set("Update")

        if self.usertype == "admin":
            delete_text = tk.StringVar()
            self.delete_btn = tk.Button(master, textvariable=delete_text, height=self.button_height,
                                        width=self.button_width, command=lambda: self.clear_window_admin(self.master))
            self.delete_btn.place(x=int(self.window_width / 2), y=int(self.window_height / 2))
            delete_text.set("Delete")

        back_text = tk.StringVar()
        self.back_btn = tk.Button(master, textvariable=back_text, height=4, width=25,
                                  command=lambda: self.back_first(self.master))
        self.back_btn.place(x=20, y=620)
        back_text.set("Back")

        logout_text = tk.StringVar()
        self.logout_btn = tk.Button(master, textvariable=logout_text,
                                    command=lambda: self.logout(self.master))
        self.logout_btn.place(x=self.window_width - 90, y=640)
        logout_text.set("Logout")


        '''
        # Employee Button
        employee_text = tk.StringVar()
        self.emp_btn = tk.Button(master, textvariable=employee_text, height=self.button_height,
                                 width=self.button_width - 50,
                                 command=lambda: function_page(self, master, "employee"))
        self.emp_btn.place(x=0, y=50)
        employee_text.set("Employee")

        # Student Button
        student_text = tk.StringVar()
        self.stu_btn = tk.Button(master, textvariable=student_text, height=self.button_height,
                                 width=self.button_width - 50,
                                 command=lambda: function_page(self, master, "student"))
        self.stu_btn.place(x=300, y=50)
        student_text.set("Student")

        # Admin Button
        admin_text = tk.StringVar()
        self.ad_btn = tk.Button(master, textvariable=admin_text, height=self.button_height,
                                width=self.button_width - 50, command=lambda: function_page(self, master, "admin"))
        self.ad_btn.place(x=600, y=50)
        admin_text.set("Admin")
        '''
    def view_window(self, master):
        # if self.usertype == "admin":
        self.create_btn.destroy()
        self.read_btn.destroy()
        # self.update_btn.destroy()
        self.delete_btn.destroy()
        self.view_btn.destroy()

        self.stat_label = tk.Label(master, text="Statistical Report", font=("Times New Roman", 20))
        self.stat_label.place(x=self.window_width/2 - 60, y=0)
        #Display number of users total, admins, student users, and employee users
        mycursor.execute('SELECT COUNT(*) FROM foodapp.DB_USER ')
        row = mycursor.fetchone()
        self.numu_str = tk.StringVar()
        self.numu_label = tk.Label(master, textvariable=self.numu_str, font=("Times New Roman", 14), fg="#861F41")
        self.numu_label.place(x= 5, y= 40)
        totalUsers = str(row[0])
        self.numu_str.set("Total Number of Users: " + totalUsers)

        mycursor.execute('SELECT COUNT(*) FROM foodapp.DB_USER WHERE UserType=1')
        arow = mycursor.fetchone()
        totalAdmins = str(arow[0])
        self.numa_str = tk.StringVar()
        self.numa_label = tk.Label(master, textvariable=self.numa_str, font=("Times New Roman", 12))
        self.numa_label.place(x=5, y=80)
        self.numa_str.set("Total Number of Admin Users: " + totalAdmins)

        mycursor.execute('SELECT COUNT(*) FROM foodapp.DB_USER WHERE UserType=2')
        erow = mycursor.fetchone()
        totalEmployee = str(erow[0])
        self.nume_str = tk.StringVar()
        self.nume_label = tk.Label(master, textvariable=self.nume_str, font=("Times New Roman", 12))
        self.nume_label.place(x=5, y=100)
        self.nume_str.set("Total Number of Employee Users: " + totalEmployee)

        mycursor.execute('SELECT COUNT(*) FROM foodapp.DB_USER WHERE UserType=3')
        srow = mycursor.fetchone()
        totalStudent = str(srow[0])
        self.nums_str = tk.StringVar()
        self.nums_label = tk.Label(master, textvariable=self.nums_str, font=("Times New Roman", 12))
        self.nums_label.place(x=5, y=120)
        self.nums_str.set("Total Number of Student Users: " + totalStudent)

        # Get dh_order stats
        mycursor.execute('SELECT COUNT(*) FROM foodapp.DH_ORDER')
        orderrow = mycursor.fetchone()
        totalOrder = str(orderrow[0])
        self.numo_str = tk.StringVar()
        self.numo_label = tk.Label(master, textvariable=self.numo_str, font=("Times New Roman", 14), fg="#861F41")
        self.numo_label.place(x=270, y=40)
        self.numo_str.set("Total Number of Orders: " + totalOrder)

        mycursor.execute('SELECT Restaurant_ID FROM foodapp.dh_order GROUP BY Restaurant_ID ORDER BY COUNT(*) DESC LIMIT 1')
        mostOrderrow = mycursor.fetchone()
        mostOrder = str(mostOrderrow[0])
        self.mosto_str = tk.StringVar()
        self.mosto_label = tk.Label(master, textvariable=self.mosto_str, font=("Times New Roman", 12))
        self.mosto_label.place(x=270, y=80)
        mycursor.execute('SELECT RestaurantName FROM foodapp.restaurant WHERE Restaurant_ID=%s', (mostOrder,))
        nameMostrow = mycursor.fetchone()
        nameMost = str(nameMostrow[0])
        self.mosto_str.set("Restaurant w/ Most Orders: " +nameMost)

        mycursor.execute('SELECT COUNT(Restaurant_ID) FROM foodapp.dh_order GROUP BY Restaurant_ID ORDER BY COUNT(*) DESC LIMIT 1')
        row = mycursor.fetchone()
        numMostOrder = str(row[0])
        self.numMost_str = tk.StringVar()
        self.numMost_label = tk.Label(master, textvariable=self.numMost_str, font=("Times New Roman", 12))
        self.numMost_label.place(x=270, y=100)
        self.numMost_str.set("Number of Most Orders: " + numMostOrder)

        mycursor.execute(
            'SELECT COUNT(Restaurant_ID) FROM foodapp.dh_order GROUP BY Restaurant_ID ORDER BY COUNT(*) LIMIT 1')
        row = mycursor.fetchone()
        numLeastOrder = str(row[0])
        self.numLeast_str = tk.StringVar()
        self.numLeast_label = tk.Label(master, textvariable=self.numLeast_str, font=("Times New Roman", 12))
        self.numLeast_label.place(x=270, y=120)
        self.numLeast_str.set("Number of Least Orders: " + numLeastOrder)

        #Number of Dining Hall
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.dining_hall')
        row = mycursor.fetchone()
        numDH = str(row[0])
        self.numDH_str = tk.StringVar()
        self.numDH_label = tk.Label(master, textvariable=self.numDH_str, font=("Times New Roman", 14), fg="#861F41")
        self.numDH_label.place(x=5, y=200)
        self.numDH_str.set("Number of Dining Halls: " + numDH)

        # Number of Dining Plan
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.dining_plan')
        row = mycursor.fetchone()
        numDP = str(row[0])
        self.numDP_str = tk.StringVar()
        self.numDP_label = tk.Label(master, textvariable=self.numDP_str, font=("Times New Roman", 14), fg="#861F41")
        self.numDP_label.place(x=270, y=200)
        self.numDP_str.set("Number of Existing Dining Plans: " + numDP)

        mycursor.execute(
            'SELECT MaxAmt FROM foodapp.dining_plan ORDER BY MaxAmt DESC LIMIT 1')
        row = mycursor.fetchone()
        maxDP = str(row[0])
        self.maxDP_str = tk.StringVar()
        self.maxDP_label = tk.Label(master, textvariable=self.maxDP_str, font=('Times New Roman', 12))
        self.maxDP_label.place(x=270, y=240)
        self.maxDP_str.set("Max Amount on Dining Plan: $" + maxDP)

        #Number of Employee
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.employee')
        row = mycursor.fetchone()
        numEmp = str(row[0])
        self.numEmp_str = tk.StringVar()
        self.numEmp_label = tk.Label(master, textvariable=self.numEmp_str, font=("Times New Roman", 14), fg="#861F41")
        self.numEmp_label.place(x=5, y=300)
        self.numEmp_str.set("Number of Employees: " + numEmp)

        mycursor.execute(
            'SELECT Days_Worked FROM foodapp.employee ORDER BY Days_Worked DESC LIMIT 1')
        row = mycursor.fetchone()
        mostDay = str(row[0])
        self.mostDay_str = tk.StringVar()
        self.mostDay_label = tk.Label(master, textvariable=self.mostDay_str, font=("Times New Roman", 12))
        self.mostDay_label.place(x=5, y=340)
        self.mostDay_str.set("Most Days Worked By Employee in\n a Week: " + mostDay + " days")

        mycursor.execute(
            'SELECT Hours_Worked FROM foodapp.employee ORDER BY Hours_Worked DESC LIMIT 1')
        row = mycursor.fetchone()
        mostHours = str(row[0])
        self.mostHours_str = tk.StringVar()
        self.mostHours_label = tk.Label(master, textvariable=self.mostHours_str, font=("Times New Roman", 12))
        self.mostHours_label.place(x=5, y=380)
        self.mostHours_str.set("Most Days Hours By Employee in\n a Week: " + mostHours + " hours")

        # Number of food_item
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.food_item')
        row = mycursor.fetchone()
        numFood = str(row[0])
        self.numFood_str = tk.StringVar()
        self.numFood_label = tk.Label(master, textvariable=self.numFood_str, font=("Times New Roman", 14), fg="#861F41")
        self.numFood_label.place(x=270, y=300)
        self.numFood_str.set("Number of Food Items: " + numFood)

        mycursor.execute(
            'SELECT AvgGrade FROM foodapp.food_item ORDER BY AvgGrade DESC LIMIT 1')
        row = mycursor.fetchone()
        highGrade = str(row[0])
        self.highGrade_str = tk.StringVar()
        self.highGrade_label = tk.Label(master, textvariable=self.highGrade_str, font=("Times New Roman", 12))
        self.highGrade_label.place(x=270, y=340)
        self.highGrade_str.set("Highest Avg. Graded Food: " + highGrade)

        mycursor.execute(
            'SELECT FoodName FROM foodapp.food_item ORDER BY AvgGrade DESC LIMIT 1')
        row = mycursor.fetchone()
        foodName = str(row[0])
        self.foodName_str = tk.StringVar()
        self.foodName_label = tk.Label(master, textvariable=self.foodName_str, font=("Times New Roman", 12))
        self.foodName_label.place(x=270, y=365)
        self.foodName_str.set("Name of Food Item w/ Highest Avg. Grade: " + foodName.capitalize())

        #Number of Forum
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.forum')
        row = mycursor.fetchone()
        forumNum = str(row[0])
        self.forumNum_str = tk.StringVar()
        self.forumNum_label = tk.Label(master, textvariable=self.forumNum_str, font=("Times New Roman", 14), fg="#861F41")
        self.forumNum_label.place(x=5, y=440)
        self.forumNum_str.set("Number of Forum Posts: " + forumNum)

        mycursor.execute(
            'SELECT PID, COUNT(PID) as Count FROM foodapp.forum GROUP BY PID ORDER BY COUNT(*) DESC LIMIT 1')
        row = mycursor.fetchone()
        mostForum = str(row[0])

        self.mostForum_str = tk.StringVar()
        self.mostForum_label = tk.Label(master, textvariable=self.mostForum_str, font=("Times New Roman", 12))
        self.mostForum_label.place(x=5, y=480)
        self.mostForum_str.set("Most Number of Forum Posts by a User: " + mostForum)

        mycursor.execute(
            'SELECT FoodItemID FROM foodapp.forum GROUP BY FoodItemID ORDER BY COUNT(*) DESC LIMIT 1')
        row = mycursor.fetchone()
        mostRFood = str(row[0])
        mycursor.execute('SELECT FoodName FROM foodapp.food_item WHERE FoodItem_ID = %s', (mostRFood,))
        row = mycursor.fetchone()
        mostRFoodName = str(row[0])

        self.mostRFood_str = tk.StringVar()
        self.mostRFood_label = tk.Label(master, textvariable=self.mostRFood_str, font=("Times New Roman", 12))
        self.mostRFood_label.place(x=5, y=500)
        self.mostRFood_str.set("Most Posted Food Item: " + mostRFoodName.capitalize())

        #Number of Restaurants

        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.restaurant')
        row = mycursor.fetchone()
        numRes = str(row[0])
        self.numRes_str = tk.StringVar()
        self.numRes_label = tk.Label(master, textvariable=self.numRes_str, font=("Times New Roman", 14), fg="#861F41")
        self.numRes_label.place(x=600, y=40)
        self.numRes_str.set("Number of Restaurants: " + numRes)

        mycursor.execute(
            'SELECT Dining_Hall_ID FROM foodapp.restaurant GROUP BY Dining_Hall_ID ORDER BY COUNT(*) DESC LIMIT 1')
        row = mycursor.fetchone()
        mostRDH = str(row[0])
        mycursor.execute('SELECT DH_Name FROM foodapp.dining_hall WHERE Dining_Hall_ID = %s', (mostRDH,))
        row = mycursor.fetchone()
        mostRDHName = str(row[0])

        self.mostRDHName_str = tk.StringVar()
        self.mostRDHName_label = tk.Label(master, textvariable=self.mostRDHName_str, font=("Times New Roman", 12))
        self.mostRDHName_label.place(x=600, y=80)
        self.mostRDHName_str.set("Dining Hall w/ Most Restaurants: " + mostRDHName.capitalize())

        #Number of Students
        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.student')
        row = mycursor.fetchone()
        numStu = str(row[0])
        self.numStu_str = tk.StringVar()
        self.numStu_label = tk.Label(master, textvariable=self.numStu_str, font=("Times New Roman", 14), fg="#861F41")
        self.numStu_label.place(x=600, y=200)
        self.numStu_str.set("Number of Students: " + numStu)


        mycursor.execute(
            'SELECT MAX(TotalMoney) FROM foodapp.student')
        row = mycursor.fetchone()
        mostMoney = str(row[0])
        self.mostMoney_str = tk.StringVar()
        self.mostMoney_label = tk.Label(master, textvariable=self.mostMoney_str, font=("Times New Roman", 12))
        self.mostMoney_label.place(x=600, y=240)
        self.mostMoney_str.set("Number of Students: " + mostMoney)

        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.student WHERE Grade="Freshman"')
        row = mycursor.fetchone()
        numFre = str(row[0])
        self.numFre_str = tk.StringVar()
        self.numFre_label = tk.Label(master, textvariable=self.numFre_str, font=("Times New Roman", 12))
        self.numFre_label.place(x=600, y=260)
        self.numFre_str.set("Number of Freshman w/ Dining Plan: " + numFre)

        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.student WHERE Grade="Sophomore"')
        row = mycursor.fetchone()
        numSop = str(row[0])
        self.numSop_str = tk.StringVar()
        self.numSop_label = tk.Label(master, textvariable=self.numSop_str, font=("Times New Roman", 12))
        self.numSop_label.place(x=600, y=280)
        self.numSop_str.set("Number of Sophomore w/ Dining Plan: " + numSop)

        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.student WHERE Grade="Sophomore"')
        row = mycursor.fetchone()
        numJun = str(row[0])
        self.numJun_str = tk.StringVar()
        self.numJun_label = tk.Label(master, textvariable=self.numJun_str, font=("Times New Roman", 12))
        self.numJun_label.place(x=600, y=300)
        self.numJun_str.set("Number of Junior w/ Dining Plan: " + numJun)

        mycursor.execute(
            'SELECT COUNT(*) FROM foodapp.student WHERE Grade="Senior"')
        row = mycursor.fetchone()
        numSen = str(row[0])
        self.numSen_str = tk.StringVar()
        self.numSen_label = tk.Label(master, textvariable=self.numSen_str, font=("Times New Roman", 12))
        self.numSen_label.place(x=600, y=320)
        self.numSen_str.set("Number of Senior w/ Dining Plan: " + numSen)




    def create_window(self, master):
        print("in create window")
        if self.usertype == "admin":
            self.create_btn.destroy()
            self.read_btn.destroy()
            self.view_btn.destroy()
            #self.update_btn.destroy()
            if self.usertype == "admin":
                self.delete_btn.destroy()

            admin_create_food_str = tk.StringVar()
            self.admin_create_food_btn = tk.Button(master, textvariable=admin_create_food_str,
                                                   height=self.button_height, width=self.button_width,
                                                   command=lambda: self.insert_admin(self.master, "food"))
            self.admin_create_food_btn.place(x=0, y=0)
            admin_create_food_str.set("Food")

            admin_create_dining_hall_str = tk.StringVar()
            self.admin_create_dining_hall_btn = tk.Button(master, textvariable=admin_create_dining_hall_str,
                                                          height=self.button_height, width=self.button_width,
                                                          command=lambda: self.insert_admin(self.master, "hall"))
            self.admin_create_dining_hall_btn.place(x=0, y=self.window_height / 2)
            admin_create_dining_hall_str.set("Dining Hall")

            admin_create_restaurant_str = tk.StringVar()
            self.admin_create_restaurant_btn = tk.Button(master, textvariable=admin_create_restaurant_str,
                                                         height=self.button_height, width=self.button_width,
                                                         command=lambda: self.insert_admin(self.master, "restaurant"))
            self.admin_create_restaurant_btn.place(x=self.window_width / 2, y=0)
            admin_create_restaurant_str.set("Restaurant")

            admin_create_admin_str = tk.StringVar()
            self.admin_create_admin_btn = tk.Button(master, textvariable=admin_create_admin_str,
                                                    height=self.button_height, width=self.button_width,
                                                    command=lambda: self.insert_admin_yb(self.master, "admin"))
            self.admin_create_admin_btn.place(x=self.window_width / 2, y=self.window_height / 2)
            admin_create_admin_str.set("Admins")

            # food_text = tk.StringVar()
            # self.food_btn = tk.Button(master, textvariable=food_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("food", master))
            # self.food_btn.place(x=0, y=0)
            # food_text.set("Food Information + Reviews")
            #
            # dining_hall_text = tk.StringVar()
            # self.dining_hall_btn = tk.Button(master, textvariable=dining_hall_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("hall", master))
            # self.dining_hall_btn.place(x=int(self.window_width / 4) + 68, y=0)
            # dining_hall_text.set("Dining Hall Information + Reviews")
            #
            # employee_text = tk.StringVar()
            # self.employee_btn = tk.Button(master, textvariable=employee_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("employee", master))
            # self.employee_btn.place(x=3 * (int(self.window_width / 4) - 42), y = 0)
            # employee_text.set("Employee Information")
            #
            # all_item_text = tk.StringVar()
            # self.all_item_btn = tk.Button(master, textvariable=all_item_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("all", master))
            # self.all_item_btn.place(x = 0, y = int(self.window_height / 2))
            # all_item_text.set("All Reviews")
            #
            # restaurant_text = tk.StringVar()
            # self.restaurant_btn = tk.Button(master, textvariable=restaurant_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("restaurant", master))
            # self.restaurant_btn.place(x=2 * int(self.window_width / 4) - 181, y= 1 * int(self.window_height / 2))
            # restaurant_text.set("Reviews by Restaurant")
            #
            # student_text = tk.StringVar()
            # self.student_btn = tk.Button(master, textvariable=student_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("student", master))
            # self.student_btn.place(x=3 * int(self.window_width / 4) - 120, y= 1 * int(self.window_height / 2))
            # student_text.set("Reviews by Student")
        if self.usertype == "student":
            self.create_btn.destroy()
            self.read_btn.destroy()
            #self.update_btn.destroy()
            if self.usertype == "admin":
                self.delete_btn.destroy()

            self.first_name_text_label = tk.StringVar()
            self.first_name_label = tk.Label(master, textvariable=self.first_name_text_label, pady=10)
            self.first_name_label.place(x=100, y=0)
            self.first_name_text_label.set("First Name:")

            self.first_name = tk.Text(master, height=0, width=35, padx=5, pady=2)
            self.first_name.place(x=100, y=40)

            self.last_name_text_label = tk.StringVar()
            self.last_name_label = tk.Label(master, textvariable=self.last_name_text_label, pady=10)
            self.last_name_label.place(x=550, y=0)
            self.last_name_text_label.set("Last Name:")

            self.last_name = tk.Text(master, height=0, width=35, padx=5, pady=2)
            self.last_name.place(x=550, y=40)

            self.food_name_text_label = tk.StringVar()
            self.food_name_label = tk.Label(master, textvariable=self.food_name_text_label, pady=10)
            self.food_name_label.place(x=100, y=60)
            self.food_name_text_label.set("Name of Food:")

            self.food_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.food_name.place(x=100, y=90)

            # self.restaurant_text_label = tk.StringVar()
            # self.restaurant_label = tk.Label(master, textvariable = self.restaurant_text_label, pady = 7)
            # self.restaurant_label.place(x = 100, y = 115)
            # self.restaurant_text_label.set("Name of Restaurant:")
            #
            # self.restaurant = tk.Text(master, height = 0, width = 91, padx = 5, pady = 2)
            # self.restaurant.place(x=100, y = 150)

            ins_text = tk.StringVar()
            self.insert_btn = tk.Button(master, textvariable=ins_text, height=3, width=30,
                                        command=lambda: self.insert_entity_make_post(master))
            self.insert_btn.place(x=350, y=200)
            ins_text.set("Insert Food")

            back_text = tk.StringVar()
            self.back_btn = tk.Button(master, textvariable=back_text, height=4, width=25,
                                      command=lambda: self.back_first(master))
            self.back_btn.place(x=20, y=620)
            back_text.set("Back")

        if self.usertype == "employee":

            if self.password_checkbutton is not None:
                self.password_checkbutton.destroy()





    def create_window_2(self, master):
        if self.usertype == "employee":

            if self.password_checkbutton is not None:
                self.password_checkbutton.destroy()

            self.create_btn.destroy()
            self.read_btn.destroy()
            self.user_input_m_text_label = tk.StringVar()
            self.user_input_m_label = tk.Label(master, textvariable=self.user_input_m_text_label, pady=10)
            self.user_input_m_label.place(x=100, y=0)
            self.user_input_m_text_label.set("Enter Employee's First Name:")

            self.m_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.m_item_name.tag_configure("center", justify='center')
            self.m_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_mm_text_label = tk.StringVar()
            self.user_input_mm_label = tk.Label(master, textvariable=self.user_input_mm_text_label, pady=10)
            self.user_input_mm_label.place(x=100, y=80)
            self.user_input_mm_text_label.set("Enter Employee's Middle Initial:")

            self.mm_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.mm_item_name.tag_configure("center", justify='center')
            self.mm_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_mmm_text_label = tk.StringVar()
            self.user_input_mmm_label = tk.Label(master, textvariable=self.user_input_mmm_text_label, pady=10)
            self.user_input_mmm_label.place(x=100, y=160)
            self.user_input_mmm_text_label.set("Enter Employee's Last Name:")

            self.mmm_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.mmm_item_name.tag_configure("center", justify='center')
            self.mmm_item_name.place(x=100, y=200)

            # days worked
            self.days_worked_text_label = tk.StringVar()
            self.days_worked_label = tk.Label(master, textvariable=self.days_worked_text_label, pady=10)
            self.days_worked_label.place(x=100, y=240)
            self.days_worked_text_label.set("Enter Number of Days Worked: ")

            self.days_worked_text = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.days_worked_text.tag_configure("center", justify='center')
            self.days_worked_text.place(x=100, y=280)

            # hours worked
            self.hours_worked_text_label = tk.StringVar()
            self.hours_worked_label = tk.Label(master, textvariable=self.hours_worked_text_label, pady=10)
            self.hours_worked_label.place(x=100, y=320)
            self.hours_worked_text_label.set("Enter Hours Worked: ")

            self.hours_worked_text = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.hours_worked_text.tag_configure("center", justify='center')
            self.hours_worked_text.place(x=100, y=360)

            self.location_worked_text_label = tk.StringVar()
            self.location_worked_label = tk.Label(master, textvariable=self.location_worked_text_label, pady=10)
            self.location_worked_label.place(x=100, y=400)
            self.location_worked_text_label.set("Enter Restaurant Worked: ")

            self.location_worked_text = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.location_worked_text.tag_configure("center", justify='center')
            self.location_worked_text.place(x=100, y=440)

            self.nextt_m_text = tk.StringVar()
            self.nextt_m_text_btn = tk.Button(master, textvariable=self.nextt_m_text, height=1, width=10,
                                              command=lambda: self.employee_insert_hours(master))
            self.nextt_m_text_btn.place(x=475, y=500)
            self.nextt_m_text.set("Next")

        if self.usertype == "admin":
            self.create_btn.destroy()
            self.read_btn.destroy()
            self.user_input_m_text_label = tk.StringVar()
            self.user_input_m_label = tk.Label(master, textvariable=self.user_input_m_text_label, pady=10)
            self.user_input_m_label.place(x=100, y=0)
            self.user_input_m_text_label.set("Enter Employee's First Name:")

            self.m_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.m_item_name.tag_configure("center", justify='center')
            self.m_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_mm_text_label = tk.StringVar()
            self.user_input_mm_label = tk.Label(master, textvariable=self.user_input_mm_text_label, pady=10)
            self.user_input_mm_label.place(x=100, y=80)
            self.user_input_mm_text_label.set("Enter Employee's Middle Initial:")

            self.mm_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.mm_item_name.tag_configure("center", justify='center')
            self.mm_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_mmm_text_label = tk.StringVar()
            self.user_input_mmm_label = tk.Label(master, textvariable=self.user_input_mmm_text_label, pady=10)
            self.user_input_mmm_label.place(x=100, y=160)
            self.user_input_mmm_text_label.set("Enter Employee's Last Name:")

            self.mmm_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.mmm_item_name.tag_configure("center", justify='center')
            self.mmm_item_name.place(x=100, y=200)

            #days worked
            self.days_worked_text_label = tk.StringVar()
            self.days_worked_label = tk.Label(master, textvariable = self.days_worked_text_label, pady=10)
            self.days_worked_label.place(x=100, y = 240)
            self.days_worked_text_label.set("Enter Number of Days Worked: ")

            self.days_worked_text = tk.Text(master, height=0, width=100, padx=15, pady =10)
            self.days_worked_text.tag_configure("center", justify='center')
            self.days_worked_text.place(x=100, y=280)

            #hours worked
            self.hours_worked_text_label = tk.StringVar()
            self.hours_worked_label = tk.Label(master, textvariable=self.hours_worked_text_label, pady=10)
            self.hours_worked_label.place(x=100, y=320)
            self.hours_worked_text_label.set("Enter Hours Worked: ")

            self.hours_worked_text = tk.Text(master, height=0, width=100, padx=15, pady= 10)
            self.hours_worked_text.tag_configure("center", justify='center')
            self.hours_worked_text.place(x=100, y = 360)

            self.location_worked_text_label = tk.StringVar()
            self.location_worked_label = tk.Label(master, textvariable = self.location_worked_text_label, pady=10)
            self.location_worked_label.place(x=100, y=400)
            self.location_worked_text_label.set("Enter Restaurant Worked: ")

            self.location_worked_text = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.location_worked_text.tag_configure("center", justify='center')
            self.location_worked_text.place(x=100, y=440)

            self.nextt_m_text = tk.StringVar()
            self.nextt_m_text_btn = tk.Button(master, textvariable=self.nextt_m_text, height=1, width=10,
                                              command=lambda: self.employee_insert_hours(master))
            self.nextt_m_text_btn.place(x=475, y=500)
            self.nextt_m_text.set("Next")

    def employee_insert_hours(self, master):
        first_n = self.m_item_name.get('1.0', 'end-1c')
        middle_i = self.mm_item_name.get('1.0', 'end-1c')
        last_n = self.mmm_item_name.get('1.0', 'end-1c')
        days_w = self.days_worked_text.get('1.0', 'end-1c')
        hours_w = self.hours_worked_text.get('1.0', 'end-1c')
        location_w = self.location_worked_text.get('1.0', 'end-1c')
        location_id = 0
        mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (location_w,))
        for i in mycursor:
            location_id = i[0]
        mycursor.execute("INSERT INTO foodapp.EMPLOYEE VALUES(%s, %s, %s, %s, %s, %s, %s);", (self.employee_index, first_n, middle_i, last_n, days_w, hours_w, location_id))
        mydb.commit()
        #hiii

    def clear_window_1(self, clicked, master):
        print("in cw")
        #additions heere
        if clicked == "all":
            if self.usertype == "admin":
                self.view_btn.destroy()
            # self.food_clicked = True
            print("in all")

            self.all_text  = tk.StringVar()
            all_text_label = tk.Label(master, textvariable= self.all_text, pady=20)
            all_text_label.place(x = 100, y = 0)

            db_one = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
            mycursor_one = db_one.cursor()
            mycursor_one.execute("SELECT ForumID, STUDENT.FName,STUDENT.MName, STUDENT.LName, Content as Review, Stars, FoodName, RESTAURANT.RestaurantName FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID; ")
            all_string = ""
            for i in mycursor_one:
                #print(str(i) + "break")
                all_string = all_string + str(i) + '\n'
            print(all_string)
            self.all_text.set("ForumID, FName, MName, LName, Review, Stars, FoodName, RestaurantName\n\n" + all_string)
           # for token in all_string:



            # --------------------------------------------- SCROLLBAR STUFF (i took it out for now) -------------------------------------------
            # roott = Tk()
            # scrollbar = Scrollbar(roott)
            # scrollbar.pack(side=RIGHT, fill=Y)
            #
            # mylist = Listbox(roott, yscrollcommand=scrollbar.set)
            # for line in all_string:
            #     mylist.insert(END, line)
            #
            # mylist.pack(side=LEFT, fill=BOTH)
            # scrollbar.config(command=mylist.xview)

        elif clicked == "hall":

            if self.usertype == "admin":
                self.view_btn.destroy()
            self.dining_hall_clicked = True
            self.view_text.destroy()
            self.view_btn.destroy()

            print("in dining hall info + reviews")
            self.user_input_dh_text_label = tk.StringVar()
            self.user_input_dh_label = tk.Label(master, textvariable=self.user_input_dh_text_label, pady=10)
            self.user_input_dh_label.place(x=100, y=0)
            self.user_input_dh_text_label.set("Enter Dining Hall to be Searched For:")

            self.dh_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.dh_item_name.tag_configure("center", justify='center')
            self.dh_item_name.place(x=100, y=40)

            self.nextt_dh_text = tk.StringVar()
            self.nextt_dh_text_btn = tk.Button(master, textvariable=self.nextt_dh_text, height=1, width=10,
                                            command=lambda: self.clear_next_button_dh(master))
            self.nextt_dh_text_btn.place(x=475, y=270)
            self.nextt_dh_text.set("Next")
        elif clicked == "employee":
            self.employee_clicked = True
            print("in employee")
            # First Name
            self.user_input_e_text_label = tk.StringVar()
            self.user_input_e_label = tk.Label(master, textvariable=self.user_input_e_text_label, pady=10)
            self.user_input_e_label.place(x=100, y=0)
            self.user_input_e_text_label.set("Enter Employee's First Name:")

            self.e_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.e_item_name.tag_configure("center", justify='center')
            self.e_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_ee_text_label = tk.StringVar()
            self.user_input_ee_label = tk.Label(master, textvariable=self.user_input_ee_text_label, pady=10)
            self.user_input_ee_label.place(x=100, y=80)
            self.user_input_ee_text_label.set("Enter Employee's Middle Name:")

            self.ee_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.ee_item_name.tag_configure("center", justify='center')
            self.ee_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_eee_text_label = tk.StringVar()
            self.user_input_eee_label = tk.Label(master, textvariable=self.user_input_eee_text_label, pady=10)
            self.user_input_eee_label.place(x=100, y=160)
            self.user_input_eee_text_label.set("Enter Employee's Last Name:")

            self.eee_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.eee_item_name.tag_configure("center", justify='center')
            self.eee_item_name.place(x=100, y=200)

            self.nextt_e_text = tk.StringVar()
            self.nextt_e_text_btn = tk.Button(master, textvariable=self.nextt_e_text, height=1, width=10,
                                              command=lambda: self.clear_next_button_e(master))
            self.nextt_e_text_btn.place(x=475, y=270)
            self.nextt_e_text.set("Next")
        elif clicked == "food":
            self.food_item_clicked = True
            print("in food")
            self.user_input_text_label = tk.StringVar()
            self.user_input__label = tk.Label(master, textvariable=self.user_input_text_label, pady=10)
            self.user_input__label.place(x=100, y=0)
            self.user_input_text_label.set("Enter Food Item to be Searched For:")

            self.food_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.food_item_name.tag_configure("center", justify='center')
            self.food_item_name.place(x=100, y=40)

            self.nextt_text = tk.StringVar()
            self.nextt_text_btn = tk.Button(master, textvariable= self.nextt_text, height=1, width=10,
                                   command=lambda: self.clear_next_button(master))
            self.nextt_text_btn.place(x=475, y=270)
            self.nextt_text.set("Next")

        elif clicked == "restaurant":
            self.restaurant_clicked = True
            self.user_input_r_text_label = tk.StringVar()
            self.user_input_r_label = tk.Label(master, textvariable=self.user_input_r_text_label, pady=10)
            self.user_input_r_label.place(x=100, y=0)
            self.user_input_r_text_label.set("Enter Restaurant Item to be Searched For:")

            self.r_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.r_item_name.tag_configure("center", justify='center')
            self.r_item_name.place(x=100, y=40)

            self.nextt_r_text = tk.StringVar()
            self.nextt_r_text_btn = tk.Button(master, textvariable=self.nextt_r_text, height=1, width=10,
                                            command=lambda: self.clear_next_button_r(master))
            self.nextt_r_text_btn.place(x=475, y=270)
            self.nextt_r_text.set("Next")
            print("in restaurant")
        elif clicked == "student":
            self.student_clicked = True
            print("in student")
            # First Name
            self.user_input_s_text_label = tk.StringVar()
            self.user_input_s_label = tk.Label(master, textvariable=self.user_input_s_text_label, pady=10)
            self.user_input_s_label.place(x=100, y=0)
            self.user_input_s_text_label.set("Enter Student's First Name:")

            self.s_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.s_item_name.tag_configure("center", justify='center')
            self.s_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_ss_text_label = tk.StringVar()
            self.user_input_ss_label = tk.Label(master, textvariable=self.user_input_ss_text_label, pady=10)
            self.user_input_ss_label.place(x=100, y=80)
            self.user_input_ss_text_label.set("Enter Student's Middle Name:")

            self.ss_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.ss_item_name.tag_configure("center", justify='center')
            self.ss_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_sss_text_label = tk.StringVar()
            self.user_input_sss_label = tk.Label(master, textvariable=self.user_input_sss_text_label, pady=10)
            self.user_input_sss_label.place(x=100, y=160)
            self.user_input_sss_text_label.set("Enter Student's Last Name:")

            self.sss_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.sss_item_name.tag_configure("center", justify='center')
            self.sss_item_name.place(x=100, y=200)

            self.nextt_s_text = tk.StringVar()
            self.nextt_s_text_btn = tk.Button(master, textvariable=self.nextt_s_text, height=1, width=10,
                                              command=lambda: self.clear_next_button_s(master))
            self.nextt_s_text_btn.place(x=475, y=270)
            self.nextt_s_text.set("Next")
        self.entity = clicked

        self.food_btn.destroy()
        self.dining_hall_btn.destroy()
        self.all_item_btn.destroy()
        self.employee_btn.destroy()
        self.restaurant_btn.destroy()
        self.student_btn.destroy()
        admin_create_restaurant_str = tk.StringVar()
        self.admin_create_restaurant_btn = tk.Button(master, textvariable=admin_create_restaurant_str, height = (2 * self.button_height), width = self.button_width_2, command=lambda: self.insert_admin(master, "restaurant"))
        self.admin_create_restaurant_btn.place(x = (2 * (self.button_width + 260)), y = 0)
        admin_create_restaurant_str.set("Restaurant")

        back_text = tk.StringVar()
        self.back_btn = tk.Button(master, textvariable=back_text, height=4, width=25,
                                  command=lambda: self.back_first(master))
        self.back_btn.place(x=20, y=620)
        back_text.set("Back")



    # This is for READ student, next
    def clear_next_button_e(self, master):
       namee_e = self.e_item_name.get('1.0', 'end-1c')
       print(namee_e)
       namee_ee = self.ee_item_name.get('1.0', 'end-1c')
       print(namee_ee)
       namee_eee = self.eee_item_name.get('1.0', 'end-1c')
       print(namee_eee)

       self.user_input_e_label.destroy()
       self.e_item_name.destroy()
       self.nextt_e_text_btn.destroy()
       self.user_input_ee_label.destroy()
       self.ee_item_name.destroy()
       self.user_input_eee_label.destroy()
       self.eee_item_name.destroy()

       self.e_info_text = tk.StringVar()
       self.e_info_label = tk.Label(master, textvariable=self.e_info_text, pady=10)
       self.e_info_label.place(x=10, y=10)
       self.e_info_text.set("Employee Information:")

       db_six = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
       mycursor_six = db_six.cursor()
       mycursor_six.execute(
           "SELECT Employee.FName, MName, LName, Days_Worked, Hours_Worked, RESTAURANT.RestaurantName, DINING_HALL.DH_Name FROM Employee INNER JOIN foodapp.RESTAURANT ON Employee.Restaurant_ID = RESTAURANT.Restaurant_ID INNER JOIN foodapp.DINING_HALL ON foodapp.RESTAURANT.Dining_Hall_ID = DINING_HALL.Dining_Hall_ID WHERE Employee.FName =%s AND Employee.MName =%s AND Employee.LName =%s;",(namee_e, namee_ee, namee_eee))

       self.e_text = tk.StringVar()
       self.e_text_label = tk.Label(master, textvariable=self.e_text, pady=20)
       self.e_text_label.place(x=150, y=50)
       all_string = ""
       noMatches = False
       for i in mycursor_six:
           all_string = all_string + str(i) + '\n'
       if (all_string.strip() == ''):
           print('no matches found ')
           noMatches = True;
       else:
           print(all_string)
           all_string = "Employee Information: \n\n\n" + "FName, MName, LName, Days_Worked, Hours_Worked, RestaurantName, DH_Name\n\n\n"+  all_string
           self.e_text.set(all_string)

       self.e_info_label.destroy()
       self.e_item_name.destroy()

       if (noMatches == True):
           nm_text = tk.StringVar()
           self.nm_text_label = tk.Label(master, textvariable=nm_text, pady=10)
           self.nm_text_label.place(x=10, y=10)
           nm_text.set("No Matches Found for this Employee\n Check Syntax")
     # This is for READ student, next
    def clear_next_button_s(self, master):
       namee_s = self.s_item_name.get('1.0', 'end-1c')
       print(namee_s)
       namee_ss = self.ss_item_name.get('1.0', 'end-1c')
       print(namee_ss)
       namee_sss = self.sss_item_name.get('1.0', 'end-1c')
       print(namee_sss)

       self.user_input_s_label.destroy()
       self.s_item_name.destroy()
       self.nextt_s_text_btn.destroy()
       self.user_input_ss_label.destroy()
       self.ss_item_name.destroy()
       self.user_input_sss_label.destroy()
       self.sss_item_name.destroy()

       self.s_info_text = tk.StringVar()
       self.s_info_label = tk.Label(master, textvariable=self.r_info_text, pady=10)
       self.s_info_label.place(x=10, y=10)
       self.s_info_text.set("Food Reviews by Student:")

       db_five = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
       mycursor_five = db_five.cursor()
       mycursor_five.execute(
           "SELECT STUDENT.FName,STUDENT.MName, STUDENT.LName, ForumID, FoodName, RESTAURANT.RestaurantName, Content as Review, Stars, DINING_HALL.DH_Name FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID INNER JOIN foodapp.DINING_HALL ON foodapp.RESTAURANT.Dining_Hall_ID = DINING_HALL.Dining_Hall_ID WHERE STUDENT.FName = %s AND STUDENT.MName =%s AND STUDENT.LName =%s ORDER BY DINING_HALL.DH_Name, FoodName;",(namee_s, namee_ss, namee_sss))

       self.s_text = tk.StringVar()
       self.s_text_label = tk.Label(master, textvariable=self.s_text, pady=20)
       self.s_text_label.place(x=150, y=50)
       all_string = ""
       noMatches = False
       for i in mycursor_five:
           all_string = all_string + str(i) + '\n'
       if (all_string.strip() == ''):
           print('no matches found ')
           noMatches = True;
       else:
           print(all_string)
           all_string = "Reviews by Student: \n\n\n" + all_string
           self.s_text.set("\nFName, MName, LName, ForumID, FoodName, RestaurantName, Review, Stars, DH_Name\n\n"+ all_string)

       self.s_info_label.destroy()
       self.s_item_name.destroy()

       if (noMatches == True):
           nm_text = tk.StringVar()
           self.nm_text_label = tk.Label(master, textvariable=nm_text, pady=10)
           self.nm_text_label.place(x=10, y=10)
           nm_text.set("No Matches Found for this Student\n Check Syntax")





    # This is for READ Restaurant, next
    def clear_next_button_r(self, master):
        namee_r = self.r_item_name.get('1.0', 'end-1c')
        print(namee_r)

        self.user_input_r_label.destroy()
        self.r_item_name.destroy()
        self.nextt_r_text_btn.destroy()

        self.r_info_text = tk.StringVar()
        self.r_info_label = tk.Label(master, textvariable=self.r_info_text, pady=10)
        self.r_info_label.place(x=10, y=10)
        self.r_info_text.set("Restaurant Information:")

        # Part One: DH Information
        db_four = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
        mycursor_four= db_four.cursor()
        mycursor_four.execute(
            "SELECT RESTAURANT.RestaurantName, RESTAURANT.Hours, DINING_HALL.DH_Name FROM foodapp.RESTAURANT INNER JOIN foodapp.DINING_HALL ON foodapp.RESTAURANT.Dining_Hall_ID = DINING_HALL.Dining_Hall_ID WHERE RestaurantName = %s ORDER BY RESTAURANT.RestaurantName",(namee_r,))

        self.r_text = tk.StringVar()
        self.r_text_label = tk.Label(master, textvariable=self.r_text, pady=20)
        self.r_text_label.place(x=35, y=50)

        all_string = ""
        noMatches = False
        for i in mycursor_four:
            # print(str(i) + "break")
            all_string = all_string + str(i) + '\n'
        if (all_string.strip() == ''):
            print('no matches found ')
            noMatches = True;
        else:
            print(all_string)
            all_string = "Restaurant Information: \n\n\n" + all_string
            self.r_text.set(all_string)

        self.r_info_label.destroy()
        self.r_item_name.destroy()

        if (noMatches == True):
            nm_text = tk.StringVar()
            self.nm_text_label = tk.Label(master, textvariable=nm_text, pady=10)
            self.nm_text_label.place(x=10, y=10)
            nm_text.set("No Matches Found for this Restaurant\n Check Syntax")

        # Part Two: Reviews fcr Food Items Searched
        mycursor_four.execute("SELECT RESTAURANT.RestaurantName, FoodName, ForumID, STUDENT.FName,STUDENT.MName, STUDENT.LName, Content as Review, Stars, DINING_HALL.DH_Name FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID INNER JOIN foodapp.DINING_HALL ON foodapp.RESTAURANT.Dining_Hall_ID = DINING_HALL.Dining_Hall_ID WHERE RestaurantName = %s ORDER BY DINING_HALL.DH_Name, FoodName;",(namee_r,))

        self.rr_text = tk.StringVar()
        self.rr_text_label = tk.Label(master, textvariable=self.rr_text, pady=20)
        self.rr_text_label.place(x=420, y=50)

        all_string_two = ""
        for j in mycursor_four:
            # print(str(i) + "break")
            all_string_two = all_string_two + str(j) + '\n'
        if (all_string_two.strip() == ''):
            print('no matches found ')
        else:
            print(all_string_two)
            all_string_two = "Restaurant Food Reviews: \n\n\n" + all_string_two
            self.rr_text.set(all_string_two)

    # This is for READ dh, next
    def clear_next_button_dh(self, master):
        namee_dh = self.dh_item_name.get('1.0', 'end-1c')
        print(namee_dh)

        self.user_input_dh_label.destroy()
        self.dh_item_name.destroy()
        self.nextt_dh_text_btn.destroy()

        self.dh_info_text = tk.StringVar()
        self.dh_info_label = tk.Label(master, textvariable=self.dh_info_text, pady=10)
        self.dh_info_label.place(x=10, y=10)
        self.dh_info_text.set("Dining Hall Information:")

        # Part One: DH Information
        db_three = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
        mycursor_three = db_three.cursor()
        mycursor_three.execute(
            "SELECT DH_Name, Hours, Location FROM foodapp.DINING_HALL WHERE DH_Name = %s ORDER BY DINING_HALL.Dining_Hall_ID;",(namee_dh,))

        self.dh_text = tk.StringVar()
        self.dh_text_label = tk.Label(master, textvariable=self.dh_text, pady=20)
        self.dh_text_label.place(x=35, y=50)

        all_string = ""
        noMatches = False
        for i in mycursor_three:
            # print(str(i) + "break")
            all_string = all_string + str(i) + '\n'
        if (all_string.strip() == ''):
            print('no matches found ')
            noMatches = True;
        else:
            print(all_string)
            all_string = "Dining Hall Information: \n\n\n" + all_string
            self.dh_text.set(all_string)

        self.dh_info_label.destroy()
        self.dh_item_name.destroy()

        if (noMatches == True):
            nm_text = tk.StringVar()
            self.nm_text_label = tk.Label(master, textvariable=nm_text, pady=10)
            self.nm_text_label.place(x=10, y=10)
            nm_text.set("No Matches Found for this Dining Hall\n Check Syntax")

        # Part Two: Reviews fcr Food Items Searched
        mycursor_three.execute("SELECT DINING_HALL.DH_Name, ForumID, STUDENT.FName,STUDENT.MName, STUDENT.LName, Content as Review, Stars, FoodName, FOOD_ITEM.FoodItem_ID, RESTAURANT.RestaurantName FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID INNER JOIN foodapp.DINING_HALL ON foodapp.RESTAURANT.Dining_Hall_ID = DINING_HALL.Dining_Hall_ID WHERE DH_Name = %s ORDER BY DINING_HALL.DH_Name, RESTAURANT.RestaurantName;",(namee_dh,))

        self.dhh_text = tk.StringVar()
        self.dhh_text_label = tk.Label(master, textvariable=self.dhh_text, pady=20)
        self.dhh_text_label.place(x=420, y=50)

        all_string_two = ""
        for j in mycursor_three:
            # print(str(i) + "break")
            all_string_two = all_string_two + str(j) + '\n'
        if (all_string_two.strip() == ''):
            print('no matches found ')
        else:
            print(all_string_two)
            all_string_two = "Food Item Reviews: \n\n\n" + all_string_two
            self.dhh_text.set(all_string_two)
            print("here:")
            print(self.dhh_text_label is not None)


    # This is for READ food, next
    def clear_next_button(self, master):

        namee = self.food_item_name.get('1.0', 'end-1c')
        print(namee)

        self.nextt_text_btn.destroy()
        self.user_input__label.destroy()

        self.food_info_text = tk.StringVar()
        self.food_info_label = tk.Label(master, textvariable=self.food_info_text, pady=10)
        self.food_info_label.place(x=10, y=10)
        self.food_info_text.set("Food Item Information:")

        # Part One: Food Information
        db_two = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
        mycursor_two = db_two.cursor()
        mycursor_two.execute("SELECT FoodName, Carbs, Fat, Protein, Calories, RESTAURANT.RestaurantName FROM foodapp.FOOD_ITEM INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID WHERE FoodName = %s GROUP BY FOOD_ITEM.FoodItem_ID;" ,(namee,))
        #mydb.commit()

        self.f_text = tk.StringVar()
        self.f_text_label = tk.Label(master, textvariable=self.f_text, pady=20)
        self.f_text_label.place(x=100, y= 50)

        all_string = ""
        noMatches = False
        for i in mycursor_two:
            # print(str(i) + "break")
            all_string = all_string + str(i) + '\n'
        if(all_string.strip() == ''):
            print('no matches found ')
            noMatches = True;
        else:
            print(all_string)
            all_string = "Food Item Information: \n\n\n" + all_string
            self.f_text.set(all_string)

        self.food_info_label.destroy()
        self.food_item_name.destroy()
        if (noMatches == True):
            nm_text = tk.StringVar()
            self.nm_text_label = tk.Label(master, textvariable=nm_text, pady=10)
            self.nm_text_label.place(x=10, y=10)
            nm_text.set("No Matches Found for this Food Entity")

        # Part Two: Reviews fcr Food Items Searched
        mycursor_two.execute("SELECT ForumID, STUDENT.FName,STUDENT.MName, STUDENT.LName, Content as Review, Stars, FoodName, FOOD_ITEM.FoodItem_ID, RESTAURANT.RestaurantName FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID WHERE FoodName = %s ORDER BY FOOD_ITEM.FoodItem_ID, ForumID;"
            ,(namee,))

        self.ff_text = tk.StringVar()
        self.ff_text_label = tk.Label(master, textvariable=self.ff_text, pady=20)
        self.ff_text_label.place(x=360, y=50)

        all_string_two = ""
        for j in mycursor_two:
            # print(str(i) + "break")
            all_string_two = all_string_two + str(j) + '\n'
        if (all_string_two.strip() == ''):
            print('no matches found ')
        else:
            print(all_string_two)
            all_string_two = "Food Item Reviews: \n\n\n" + all_string_two
            self.ff_text.set("ForumID, FName, MName, LName, Review, Stars, FoodName, FoodItem_ID, RestaurantName\n\n"+ all_string_two)

    def create_entity(self, master):
        if self.entity == "hall":
            dining_hall_text_label = tk.StringVar()
            dining_hall_label = tk.Label(master, textvariable=dining_hall_text_label, pady=10)
            dining_hall_label.place(x = 100, y = 0)
            dining_hall_text_label.set("Dining Hall Name:")

    def insert_admin(self, master, clicked):
        self.admin_create_food_btn.destroy()
        self.admin_create_restaurant_btn.destroy()
        self.admin_create_dining_hall_btn.destroy()
        self.password_checkbutton.destroy()
        self.admin_create_admin_btn.destroy()

        if clicked == "food":
            self.food_name_text_label = tk.StringVar()
            self.food_name_label = tk.Label(master, textvariable=self.food_name_text_label, pady=10)
            self.food_name_label.place(x=100, y=60)
            self.food_name_text_label.set("Name of Food:")

            self.food_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.food_name.place(x=100, y=90)

            self.restaurant_text_label = tk.StringVar()
            self.restaurant_label = tk.Label(master, textvariable=self.restaurant_text_label, pady=7)
            self.restaurant_label.place(x=100, y=115)
            self.restaurant_text_label.set("Name of Restaurant:")

            self.restaurant = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.restaurant.place(x=100, y=150)

            carbs_text_label = tk.StringVar()
            self.carbs_label = tk.Label(master, textvariable=carbs_text_label, pady=10)
            self.carbs_label.place(x = 100, y = 180)
            carbs_text_label.set("Carbs: ")

            self.carbs = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.carbs.place(x=100, y = 210)

            fat_text_label = tk.StringVar()
            self.fat_label = tk.Label(master, textvariable=fat_text_label, pady=10)
            self.fat_label.place(x = 300, y = 180)
            fat_text_label.set("Fat: ")

            self.fat = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.fat.place(x=300, y = 210)

            protein_text_label = tk.StringVar()
            self.protein_label = tk.Label(master, textvariable=protein_text_label, pady = 10)
            self.protein_label.place(x = 500, y = 180)
            protein_text_label.set("Protein: ")

            self.protein = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.protein.place(x = 500, y = 210)

            calories_text_label = tk.StringVar()
            self.calories_label = tk.Label(master, textvariable = calories_text_label, pady = 10)
            self.calories_label.place(x=700, y = 180)
            calories_text_label.set("Calories:")

            self.calories = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.calories.place(x = 700, y = 210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                        command=lambda: self.admin_insert(master, "food"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")
        elif clicked == "restaurant":
            restaurant_name_label_text = tk.StringVar()
            self.restaurant_name_label = tk.Label(master, textvariable=restaurant_name_label_text, pady = 10)
            self.restaurant_name_label.place(x = 100, y = 60)
            restaurant_name_label_text.set("Name of Restaurant: ")

            self.restaurant_name = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.restaurant_name.place(x=100, y=90)

            hours_label_text = tk.StringVar()
            self.hours_label = tk.Label(master, textvariable=hours_label_text, pady = 10)
            self.hours_label.place(x = 100, y = 120)
            hours_label_text.set("Hours (D-D HH:HH AM/PM - HH:HH AM/PM)")

            self.hours = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hours.place(x=100, y=150)

            res_dining_hall_label_text = tk.StringVar()
            self.res_dining_hall_label = tk.Label(master, textvariable=res_dining_hall_label_text, pady = 10)
            self.res_dining_hall_label.place(x=100, y = 180)
            res_dining_hall_label_text.set("Residing Dining Hall: ")

            self.d_hall = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.d_hall.place(x=100, y=210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                         command=lambda: self.admin_insert(master, "restaurant"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")

        elif clicked == "hall":
            hall_label_text = tk.StringVar()
            self.hall_label = tk.Label(master, textvariable=hall_label_text, pady=10)
            self.hall_label.place(x=100, y=60)
            hall_label_text.set("Dining Hall Name: ")

            self.hall_name = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_name.place(x=100, y = 90)

            hall_hours_label_text = tk.StringVar()
            self.hall_hours_label = tk.Label(master, textvariable=hall_hours_label_text, pady = 10)
            self.hall_hours_label.place(x=100, y = 120)
            hall_hours_label_text.set("Hours (D-D HH:HH AM/PM - HH:HH AM/PM)")

            self.hall_hours = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_hours.place(x=100, y=150)

            hall_location_label_text = tk.StringVar()
            self.hall_location_label = tk.Label(master, textvariable=hall_location_label_text, pady = 10)
            self.hall_location_label.place(x=100, y=180)
            hall_location_label_text.set("Location (City): ")

            self.hall_location = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_location.place(x=100, y=210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                        command=lambda: self.admin_insert(master, "hall"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")

    def admin_insert(self, master, entity_name):
        if entity_name == "food":
            carbs_value = self.carbs.get("1.0", 'end-1c')
            fat_value = self.fat.get("1.0", 'end-1c')
            protein_value = self.protein.get("1.0", 'end-1c')
            calories_value = self.calories.get("1.0", 'end-1c')
            food_value = self.food_name.get("1.0", 'end-1c')
            restaurant_value = self.restaurant.get("1.0", 'end-1c')
            avg_grade = 0.0

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (restaurant_value, ))
                restaurant_id = 0
                for i in mycursor:
                    restaurant_id = i[0]
                mycursor.execute("INSERT INTO foodapp.FOOD_ITEM VALUES (%s, 0.0, %s, %s, %s, %s, %s, %s);", (self.food_item_index, food_value, restaurant_id, carbs_value, fat_value, protein_value, calories_value))
                mydb.commit()
        elif entity_name == "restaurant":
            restaurant_name = self.restaurant_name.get("1.0", 'end-1c')
            hours_val = self.hours.get("1.0", 'end-1c')
            d_hall_name = self.d_hall.get("1.0", 'end-1c')

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.DINING_HALL WHERE DH_Name = %s;", (d_hall_name,))
                d_hall_id = 0
                for i in mycursor:
                    d_hall_id = i[0]
                print(d_hall_id)
                mycursor.execute("INSERT INTO foodapp.RESTAURANT VALUES (%s, %s, %s, %s)", (self.dining_hall_index, restaurant_name, hours_val, d_hall_id))
                mydb.commit()
        elif entity_name =="hall":
            name_val = self.hall_name.get("1.0", 'end-1c')
            hours_val = self.hall_hours.get("1.0", 'end-1c')
            location_val = self.hall_location.get("1.0", 'end-1c')

            if self.execute_sql:
                mycursor.execute("INSERT INTO foodapp.DINING_HALL VALUES (%s, %s, 1, %s, %s);",
                                 (self.restaurant_index, name_val, hours_val, location_val))
                mydb.commit()


    def insert_entity_make_post(self, master):
        f_name = self.first_name.get("1.0", 'end-1c')
        l_name = self.last_name.get("1.0", 'end-1c')
        # restaurant_name = str(self.restaurant.get("1.0", 'end-1c'))
        food_name_text = self.food_name.get("1.0", 'end-1c')
        student_id = 0
        restaurant_id = 0
        food_name_id = 0

        if self.execute_sql:
            mycursor.execute("SELECT * FROM foodapp.STUDENT WHERE Fname = %s AND LName = %s;", (f_name, l_name))
            for i in mycursor:
                student_id = i[0]
            print(student_id)

            mycursor.execute("SELECT * FROM foodapp.FOOD_ITEM WHERE FoodName = %s", (food_name_text,))
            for i in mycursor:
                restaurant_id = i[0]

            # print(restaurant_name)
            # mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (restaurant_name, ))
            # for j in mycursor:
            #     restaurant_id = j[0]
            # print(restaurant_id)

            # mycursor.execute("INSERT INTO foodapp.DH_ORDER VALUES(%s, %s, %s);", (self.order_index, restaurant_id, student_id))
            # mydb.commit()
            # mycursor.execute("INSERT INTO foodapp.FOOD_ITEM VALUES(%s, 1.0, %s, %s)", (self.food_item_index, food_name_text, restaurant_id))
            # mydb.commit()
            # mycursor.execute("SELECT * FROM foodapp.DH_ORDER;")
            # for k in mycursor:
            #     print(k)
            # mycursor.execute("SELECT * FROM foodapp.FOOD_ITEM;")
            # for l in mycursor:
            #     print(l)

            self.order_index += 1
            self.food_item_index += 1

        self.first_name.destroy()
        self.last_name.destroy()
        self.food_name.destroy()
        #self.restaurant.destroy()

        self.first_name_label.destroy()
        self.last_name_label.destroy()
        #self.restaurant_label.destroy()
        self.food_name_label.destroy()

        self.insert_btn.destroy()

        forum_text_label = tk.StringVar()
        self.forum_label = tk.Label(master, textvariable = forum_text_label, pady = 10)
        self.forum_label.place(x = 100, y = 0)
        forum_text_label.set("Review Food Item:")

        self.forum_content = tk.Text(master, height = 10, width = 91, padx = 5, pady = 2)
        self.forum_content.place(x = 100, y = 30)

        stars_text_label = tk.StringVar()
        self.stars_label = tk.Label(master, textvariable=stars_text_label, pady = 10)
        self.stars_label.place(x = 100, y = 200)
        stars_text_label.set("Stars (1-5):")

        self.stars_content = tk.Text(master, height = 1, width = 10, padx = 5, pady = 2)
        self.stars_content.place(x = 100, y = 230)

        ins_text = tk.StringVar()
        self.insert_btn = tk.Button(master, textvariable=ins_text, height=3, width=30,
                                    command=lambda: self.insert_forum(master, student_id, restaurant_id))
        self.insert_btn.place(x=350, y=250)
        ins_text.set("Insert Forum")

    def insert_forum(self, master, student_id, food_id):
        stars = self.stars_content.get("1.0", 'end-1c')
        content = self.forum_content.get("1.0", 'end-1c')
        mycursor.execute("INSERT INTO foodapp.FORUM VALUES(%s, %s, %s, %s, %s);", (self.forum_index, student_id, food_id, content, stars))
        mydb.commit()


    def admin_read(self, master):

        self.create_btn.destroy()
        self.read_btn.destroy()
        #self.update_btn.destroy()
        if self.usertype == "admin":
            self.delete_btn.destroy()
        if self.password_checkbutton is not None:
            self.password_checkbutton.destroy()

        food_text = tk.StringVar()
        self.food_btn = tk.Button(master, textvariable=food_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("food", master))
        self.food_btn.place(x=0, y=0)
        food_text.set("Food Information + Reviews")

        dining_hall_text = tk.StringVar()
        self.dining_hall_btn = tk.Button(master, textvariable=dining_hall_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("hall", master))
        self.dining_hall_btn.place(x=int(self.window_width / 4) + 68, y=0)
        dining_hall_text.set("Dining Hall Information + Reviews")

        employee_text = tk.StringVar()
        self.employee_btn = tk.Button(master, textvariable=employee_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("employee", master))
        self.employee_btn.place(x=3 * (int(self.window_width / 4) - 42), y = 0)
        employee_text.set("Employee Information")

        all_item_text = tk.StringVar()
        self.all_item_btn = tk.Button(master, textvariable=all_item_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("all", master))
        self.all_item_btn.place(x = 0, y = int(self.window_height / 2))
        all_item_text.set("All Reviews")

        restaurant_text = tk.StringVar()
        self.restaurant_btn = tk.Button(master, textvariable=restaurant_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("restaurant", master))
        self.restaurant_btn.place(x=2 * int(self.window_width / 4) - 181, y= 1 * int(self.window_height / 2))
        restaurant_text.set("Reviews by Restaurant")

        student_text = tk.StringVar()
        self.student_btn = tk.Button(master, textvariable=student_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_2("student", master))
        self.student_btn.place(x=3 * int(self.window_width / 4) - 120, y= 1 * int(self.window_height / 2))
        student_text.set("Reviews by Student")

    def clear_window_2(self, clicked, master):
        print("in cw")
        if self.usertype == "admin":
            self.view_btn.destroy()
        if clicked == "all":
            # self.food_clicked = True
            print("in all")

            self.all_text = tk.StringVar()
            self.all_text_label = tk.Label(master, textvariable=self.all_text, pady=20)
            self.all_text_label.place(x=100, y=0)

            db_one = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
            mycursor_one = db_one.cursor()
            mycursor_one.execute(
                "SELECT ForumID, STUDENT.FName,STUDENT.MName, STUDENT.LName, Content as Review, Stars, FoodName, RESTAURANT.RestaurantName FROM foodapp.FORUM INNER JOIN foodapp.FOOD_ITEM ON foodapp.FORUM.FoodItemID = foodapp.FOOD_ITEM.FoodItem_ID INNER JOIN foodapp.STUDENT ON foodapp.FORUM.PID = foodapp.STUDENT.PID INNER JOIN foodapp.RESTAURANT ON foodapp.FOOD_ITEM.Restaurant_ID = RESTAURANT.Restaurant_ID; ")
            all_string = ""
            for i in mycursor_one:
                # print(str(i) + "break")
                all_string = all_string + str(i) + '\n'
            print(all_string)
            self.all_text.set("ForumID, FName, MName, LName, Review, Stars, FoodName, RestaurantName\n\n" + all_string)
        # for token in all_string:

        # --------------------------------------------- SCROLLBAR STUFF (i took it out for now) -------------------------------------------
        # roott = Tk()
        # scrollbar = Scrollbar(roott)
        # scrollbar.pack(side=RIGHT, fill=Y)
        #
        # mylist = Listbox(roott, yscrollcommand=scrollbar.set)
        # for line in all_string:
        #     mylist.insert(END, line)
        #
        # mylist.pack(side=LEFT, fill=BOTH)
        # scrollbar.config(command=mylist.xview)

        elif clicked == "hall":
            self.dining_hall_clicked = True
            print("in dining hall info + reviews")
            self.user_input_dh_text_label = tk.StringVar()
            self.user_input_dh_label = tk.Label(master, textvariable=self.user_input_dh_text_label, pady=10)
            self.user_input_dh_label.place(x=100, y=0)
            self.user_input_dh_text_label.set("Enter Dining Hall to be Searched For:")

            self.dh_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.dh_item_name.tag_configure("center", justify='center')
            self.dh_item_name.place(x=100, y=40)

            self.nextt_dh_text = tk.StringVar()
            self.nextt_dh_text_btn = tk.Button(master, textvariable=self.nextt_dh_text, height=1, width=10,
                                               command=lambda: self.clear_next_button_dh(master))
            self.nextt_dh_text_btn.place(x=475, y=270)
            self.nextt_dh_text.set("Next")
        elif clicked == "employee":
            self.employee_clicked = True
            print("in employee")
            # First Name
            self.user_input_e_text_label = tk.StringVar()
            self.user_input_e_label = tk.Label(master, textvariable=self.user_input_e_text_label, pady=10)
            self.user_input_e_label.place(x=100, y=0)
            self.user_input_e_text_label.set("Enter Employee's First Name:")

            self.e_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.e_item_name.tag_configure("center", justify='center')
            self.e_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_ee_text_label = tk.StringVar()
            self.user_input_ee_label = tk.Label(master, textvariable=self.user_input_ee_text_label, pady=10)
            self.user_input_ee_label.place(x=100, y=80)
            self.user_input_ee_text_label.set("Enter Employee's Middle Name:")

            self.ee_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.ee_item_name.tag_configure("center", justify='center')
            self.ee_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_eee_text_label = tk.StringVar()
            self.user_input_eee_label = tk.Label(master, textvariable=self.user_input_eee_text_label, pady=10)
            self.user_input_eee_label.place(x=100, y=160)
            self.user_input_eee_text_label.set("Enter Employee's Last Name:")

            self.eee_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.eee_item_name.tag_configure("center", justify='center')
            self.eee_item_name.place(x=100, y=200)

            self.nextt_e_text = tk.StringVar()
            self.nextt_e_text_btn = tk.Button(master, textvariable=self.nextt_e_text, height=1, width=10,
                                              command=lambda: self.clear_next_button_e(master))
            self.nextt_e_text_btn.place(x=475, y=270)
            self.nextt_e_text.set("Next")
        elif clicked == "food":
            self.food_item_clicked = True
            print("in food")
            self.user_input_text_label = tk.StringVar()
            self.user_input__label = tk.Label(master, textvariable=self.user_input_text_label, pady=10)
            self.user_input__label.place(x=100, y=0)
            self.user_input_text_label.set("Enter Food Item to be Searched For:")

            self.food_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.food_item_name.tag_configure("center", justify='center')
            self.food_item_name.place(x=100, y=40)

            self.nextt_text = tk.StringVar()
            self.nextt_text_btn = tk.Button(master, textvariable=self.nextt_text, height=1, width=10,
                                            command=lambda: self.clear_next_button(master))
            self.nextt_text_btn.place(x=475, y=270)
            self.nextt_text.set("Next")

        elif clicked == "restaurant":
            self.restaurant_clicked = True
            self.user_input_r_text_label = tk.StringVar()
            self.user_input_r_label = tk.Label(master, textvariable=self.user_input_r_text_label, pady=10)
            self.user_input_r_label.place(x=100, y=0)
            self.user_input_r_text_label.set("Enter Restaurant Item to be Searched For:")

            self.r_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.r_item_name.tag_configure("center", justify='center')
            self.r_item_name.place(x=100, y=40)

            self.nextt_r_text = tk.StringVar()
            self.nextt_r_text_btn = tk.Button(master, textvariable=self.nextt_r_text, height=1, width=10,
                                              command=lambda: self.clear_next_button_r(master))
            self.nextt_r_text_btn.place(x=475, y=270)
            self.nextt_r_text.set("Next")
            print("in restaurant")
        elif clicked == "student":
            self.student_clicked = True
            print("in student")
            # First Name
            self.user_input_s_text_label = tk.StringVar()
            self.user_input_s_label = tk.Label(master, textvariable=self.user_input_s_text_label, pady=10)
            self.user_input_s_label.place(x=100, y=0)
            self.user_input_s_text_label.set("Enter Student's First Name:")

            self.s_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.s_item_name.tag_configure("center", justify='center')
            self.s_item_name.place(x=100, y=40)

            # Middle Name
            self.user_input_ss_text_label = tk.StringVar()
            self.user_input_ss_label = tk.Label(master, textvariable=self.user_input_ss_text_label, pady=10)
            self.user_input_ss_label.place(x=100, y=80)
            self.user_input_ss_text_label.set("Enter Student's Middle Name:")

            self.ss_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.ss_item_name.tag_configure("center", justify='center')
            self.ss_item_name.place(x=100, y=120)

            # Last Name
            self.user_input_sss_text_label = tk.StringVar()
            self.user_input_sss_label = tk.Label(master, textvariable=self.user_input_sss_text_label, pady=10)
            self.user_input_sss_label.place(x=100, y=160)
            self.user_input_sss_text_label.set("Enter Student's Last Name:")

            self.sss_item_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.sss_item_name.tag_configure("center", justify='center')
            self.sss_item_name.place(x=100, y=200)

            self.nextt_s_text = tk.StringVar()
            self.nextt_s_text_btn = tk.Button(master, textvariable=self.nextt_s_text, height=1, width=10,
                                              command=lambda: self.clear_next_button_s(master))
            self.nextt_s_text_btn.place(x=475, y=270)
            self.nextt_s_text.set("Next")
        self.entity = clicked

        self.food_btn.destroy()
        self.dining_hall_btn.destroy()
        self.all_item_btn.destroy()
        self.employee_btn.destroy()
        self.restaurant_btn.destroy()
        self.student_btn.destroy()

        if self.function == "create":
            self.create_entity(self, master)

    def clear_window(self, clicked, master):

        print("in clear window!!!!")
        self.create_btn.destroy()
        self.read_btn.destroy()
        #self.update_btn.destroy()
        if self.usertype == "admin":
            self.delete_btn.destroy()

            admin_create_food_str = tk.StringVar()
            self.admin_create_food_btn = tk.Button(master, textvariable = admin_create_food_str, height=self.button_height, width=self.button_width, command=lambda: self.insert_admin(self.master, "food"))
            self.admin_create_food_btn.place(x = 0, y = 0)
            admin_create_food_str.set("Food")

            admin_create_dining_hall_str = tk.StringVar()
            self.admin_create_dining_hall_btn = tk.Button(master, textvariable = admin_create_dining_hall_str, height=self.button_height, width=self.button_width, command=lambda: self.insert_admin(self.master, "hall"))
            self.admin_create_dining_hall_btn.place(x = 0, y = self.window_height/2)
            admin_create_dining_hall_str.set("Dining Hall")

            admin_create_restaurant_str = tk.StringVar()
            self.admin_create_restaurant_btn = tk.Button(master, textvariable=admin_create_restaurant_str, height=self.button_height, width=self.button_width, command=lambda: self.insert_admin(self.master, "restaurant"))
            self.admin_create_restaurant_btn.place(x = self.window_width/2, y = 0)
            admin_create_restaurant_str.set("Restaurant")

            admin_create_admin_str = tk.StringVar()
            self.admin_create_admin_btn = tk.Button(master, textvariable=admin_create_admin_str,
                                                         height=self.button_height, width=self.button_width,
                                                         command=lambda: self.insert_admin(self.master, "admin"))
            self.admin_create_admin_btn.place(x=self.window_width/2, y=self.window_height/2)
            admin_create_admin_str.set("Admins")

        if self.usertype == "employee" or self.usertype == "student":
            self.create_btn.destroy()
            self.read_btn.destroy()
            self.update_btn.destroy()
            self.delete_btn.destroy()

            self.first_name_text_label = tk.StringVar()
            self.first_name_label = tk.Label(master, textvariable=self.first_name_text_label, pady = 10)
            self.first_name_label.place(x = 100, y = 0)
            self.first_name_text_label.set("First Name:")

            self.first_name = tk.Text(master, height = 0, width = 35, padx = 5, pady = 2)
            self.first_name.place(x = 100, y = 40)

            self.last_name_text_label = tk.StringVar()
            self.last_name_label = tk.Label(master, textvariable=self.last_name_text_label, pady = 10)
            self.last_name_label.place(x = 550, y = 0)
            self.last_name_text_label.set("Last Name:")

            self.last_name = tk.Text(master, height = 0, width = 35, padx = 5, pady = 2)
            self.last_name.place(x = 550, y = 40)

            self.food_name_text_label = tk.StringVar()
            self.food_name_label = tk.Label(master, textvariable=self.food_name_text_label, pady = 10)
            self.food_name_label.place(x = 100, y = 60)
            self.food_name_text_label.set("Name of Food:")

            self.food_name = tk.Text(master, height = 0, width = 91, padx = 5, pady = 2)
            self.food_name.place(x = 100, y = 90)

            ins_text = tk.StringVar()
            self.insert_btn = tk.Button(master, textvariable=ins_text, height = 3, width = 30, command=lambda: self.insert_entity_make_post(self.master))
            self.insert_btn.place(x=350, y = 200)
            ins_text.set("Insert Food")

            back_text = tk.StringVar()
            self.back_btn = tk.Button(master, textvariable=back_text, height=4, width=25,
                                      command=lambda: self.back_first(self.master))
            self.back_btn.place(x=20, y=620)
            back_text.set("Back")



    def insert_admin_yb(self, master, clicked):

        print("IN INSERT ADMIN YB")

        self.admin_create_food_btn.destroy()
        self.admin_create_restaurant_btn.destroy()
        self.admin_create_dining_hall_btn.destroy()
        self.admin_create_admin_btn.destroy()

        if self.update_btn is not None:
            self.update_btn.destroy()

        if self.view_btn is not None:
            self.view_btn.destroy()
        if clicked == "food":
            self.food_name_text_label = tk.StringVar()
            self.food_name_label = tk.Label(master, textvariable=self.food_name_text_label, pady=10)
            self.food_name_label.place(x=100, y=60)
            self.food_name_text_label.set("Name of Food:")

            self.food_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.food_name.place(x=100, y=90)

            self.restaurant_text_label = tk.StringVar()
            self.restaurant_label = tk.Label(master, textvariable=self.restaurant_text_label, pady=7)
            self.restaurant_label.place(x=100, y=115)
            self.restaurant_text_label.set("Name of Restaurant:")

            self.restaurant = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.restaurant.place(x=100, y=150)

            carbs_text_label = tk.StringVar()
            self.carbs_label = tk.Label(master, textvariable=carbs_text_label, pady=10)
            self.carbs_label.place(x = 100, y = 180)
            carbs_text_label.set("Carbs: ")

            self.carbs = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.carbs.place(x=100, y = 210)

            fat_text_label = tk.StringVar()
            self.fat_label = tk.Label(master, textvariable=fat_text_label, pady=10)
            self.fat_label.place(x = 300, y = 180)
            fat_text_label.set("Fat: ")

            self.fat = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.fat.place(x=300, y = 210)

            protein_text_label = tk.StringVar()
            self.protein_label = tk.Label(master, textvariable=protein_text_label, pady = 10)
            self.protein_label.place(x = 500, y = 180)
            protein_text_label.set("Protein: ")

            self.protein = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.protein.place(x = 500, y = 210)

            calories_text_label = tk.StringVar()
            self.calories_label = tk.Label(master, textvariable = calories_text_label, pady = 10)
            self.calories_label.place(x=700, y = 180)
            calories_text_label.set("Calories:")

            self.calories = tk.Text(master, height=0, width=10, padx=5, pady=2)
            self.calories.place(x = 700, y = 210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                        command=lambda: self.admin_insert(self.master, "food"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")
        elif clicked == "restaurant":
            restaurant_name_label_text = tk.StringVar()
            self.restaurant_name_label = tk.Label(master, textvariable=restaurant_name_label_text, pady = 10)
            self.restaurant_name_label.place(x = 100, y = 60)
            restaurant_name_label_text.set("Name of Restaurant: ")

            self.restaurant_name = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.restaurant_name.place(x=100, y=90)

            hours_label_text = tk.StringVar()
            self.hours_label = tk.Label(master, textvariable=hours_label_text, pady = 10)
            self.hours_label.place(x = 100, y = 120)
            hours_label_text.set("Hours (D-D HH:HH AM/PM - HH:HH AM/PM)")

            self.hours = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hours.place(x=100, y=150)

            res_dining_hall_label_text = tk.StringVar()
            self.res_dining_hall_label = tk.Label(master, textvariable=res_dining_hall_label_text, pady = 10)
            self.res_dining_hall_label.place(x=100, y = 180)
            res_dining_hall_label_text.set("Residing Dining Hall: ")

            self.d_hall = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.d_hall.place(x=100, y=210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                         command=lambda: self.admin_insert(self.master, "restaurant"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")

        elif clicked == "hall":
            hall_label_text = tk.StringVar()
            self.hall_label = tk.Label(master, textvariable=hall_label_text, pady=10)
            self.hall_label.place(x=100, y=60)
            hall_label_text.set("Dining Hall Name: ")

            self.hall_name = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_name.place(x=100, y = 90)

            hall_hours_label_text = tk.StringVar()
            self.hall_hours_label = tk.Label(master, textvariable=hall_hours_label_text, pady = 10)
            self.hall_hours_label.place(x=100, y = 120)
            hall_hours_label_text.set("Hours (D-D HH:HH AM/PM - HH:HH AM/PM)")

            self.hall_hours = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_hours.place(x=100, y=150)

            hall_location_label_text = tk.StringVar()
            self.hall_location_label = tk.Label(master, textvariable=hall_location_label_text, pady = 10)
            self.hall_location_label.place(x=100, y=180)
            hall_location_label_text.set("Location (City): ")

            self.hall_location = tk.Text(master, height=0, width = 91, padx = 5, pady = 2)
            self.hall_location.place(x=100, y=210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                        command=lambda: self.admin_insert(self.master, "hall"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Insert")
        elif clicked == "admin":

            user_label_text = tk.StringVar()
            self.user_label = tk.Label(master, textvariable=user_label_text, pady=10)
            self.user_label.place(x=100, y=60)
            user_label_text.set("Username: ")

            self.user_name = tk.Entry(master, width=50)
            self.user_name.place(x=100, y=90)

            pass_label_text = tk.StringVar()
            self.pass_label = tk.Label(master, textvariable=pass_label_text, pady=10)
            self.pass_label.place(x=100, y=120)
            pass_label_text.set("Password: ")

            self.passw = tk.Entry(master,width=50,show="*")
            self.passw.place(x=100, y=150)

            cpass_label_text = tk.StringVar()
            self.cpass_label = tk.Label(master, textvariable=cpass_label_text, pady=10)
            self.cpass_label.place(x=100, y=180)
            cpass_label_text.set("Confirm Password: ")

            self.cpassw = tk.Entry(master, width=50, show="*")
            self.cpassw.place(x=100, y=210)

            em_label_text = tk.StringVar()
            self.em_label = tk.Label(master, textvariable=em_label_text, pady=10)
            self.em_label.place(x=100, y=240)
            em_label_text.set("Email: ")

            self.em = tk.Entry(master, width=50)
            self.em.place(x=100, y=270)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                              command=lambda: self.admin_insert(self.master, "admin"))
            self.admin_insert_btn.place(x=350, y=350)
            admin_insert_text.set("Insert")

    def admin_insert(self, master, entity_name):
        if entity_name == "food":
            carbs_value = self.carbs.get("1.0", 'end-1c')
            fat_value = self.fat.get("1.0", 'end-1c')
            protein_value = self.protein.get("1.0", 'end-1c')
            calories_value = self.calories.get("1.0", 'end-1c')
            food_value = self.food_name.get("1.0", 'end-1c')
            restaurant_value = self.restaurant.get("1.0", 'end-1c')
            avg_grade = 0.0

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (restaurant_value, ))
                restaurant_id = 0
                for i in mycursor:
                    restaurant_id = i[0]
                mycursor.execute("INSERT INTO foodapp.FOOD_ITEM VALUES (%s, 0.0, %s, %s, %s, %s, %s, %s);", (self.food_item_index, food_value, restaurant_id, carbs_value, fat_value, protein_value, calories_value))
                mydb.commit()
        elif entity_name == "restaurant":
            restaurant_name = self.restaurant_name.get("1.0", 'end-1c')
            hours_val = self.hours.get("1.0", 'end-1c')
            d_hall_name = self.d_hall.get("1.0", 'end-1c')

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.DINING_HALL WHERE DH_Name = %s;", (d_hall_name,))
                d_hall_id = 0
                for i in mycursor:
                    d_hall_id = i[0]
                print(d_hall_id)
                mycursor.execute("INSERT INTO foodapp.RESTAURANT VALUES (%s, %s, %s, %s)", (self.dining_hall_index, restaurant_name, hours_val, d_hall_id))
                mydb.commit()
        elif entity_name =="hall":
            name_val = self.hall_name.get("1.0", 'end-1c')
            hours_val = self.hall_hours.get("1.0", 'end-1c')
            location_val = self.hall_location.get("1.0", 'end-1c')

            if self.execute_sql:
                mycursor.execute("INSERT INTO foodapp.DINING_HALL VALUES (%s, %s, 1, %s, %s);",
                                 (self.restaurant_index, name_val, hours_val, location_val))
                mydb.commit()

        elif entity_name == "admin":
            if (self.user_name.get() == "" or self.em.get() == "" or self.passw.get() == "" or
                    self.cpassw.get() == ""):
                messagebox.showerror("Error", "All fields are required to be filled out", parent=self.master)
            elif self.passw.get() != self.cpassw.get():
                messagebox.showerror("Error", "Passwords do not match. Please confirm your password correctly.",
                                     parent=self.master)
            else:
                mycursor.execute("SELECT * FROM foodapp.DB_USER WHERE Email=%s", (self.em.get(),))
                row = mycursor.fetchone()
                if row != None:
                    messagebox.showerror("Error", "Email already in use", parent=self.master)
                    #self.regclear() Make one for Insert
                    #self.rusername_txt.focus() Make one for Insert
                else:
                    mycursor.execute('SELECT * FROM foodapp.DB_USER WHERE Username=%s', (self.user_name.get(),))
                    row = mycursor.fetchone()
                    if row != None:
                        messagebox.showerror("Error", "Username already in use", parent=self.master)
                        #self.regclear()
                        #self.rusername_txt.focus()
                    else:
                        mycursor.execute(
                            'INSERT INTO foodapp.DB_USER (Username, Email, UserPassword, UserType) VALUES(%s,%s,%s,%s)',
                            (self.user_name.get(), self.em.get(), self.passw.get(),
                             1))
                        mydb.commit()
                        messagebox.showinfo("Success", "Register Successful", parent=self.master)
                        #self.regclear() Make one for Insert
                        #self.rusername_txt.focus() Make one for Insert


    def insert_entity_make_post(self, master):
        f_name = self.first_name.get("1.0", 'end-1c')
        l_name = self.last_name.get("1.0", 'end-1c')
        # restaurant_name = str(self.restaurant.get("1.0", 'end-1c'))
        food_name_text = self.food_name.get("1.0", 'end-1c')
        student_id = 0
        restaurant_id = 0
        food_name_id = 0

        if self.execute_sql:
            mycursor.execute("SELECT * FROM foodapp.STUDENT WHERE Fname = %s AND LName = %s;", (f_name, l_name))
            for i in mycursor:
                student_id = i[0]
            print(student_id)

            mycursor.execute("SELECT * FROM foodapp.FOOD_ITEM WHERE FoodName = %s", (food_name_text,))
            for i in mycursor:
                restaurant_id = i[0]

            # print(restaurant_name)
            # mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (restaurant_name, ))
            # for j in mycursor:
            #     restaurant_id = j[0]
            # print(restaurant_id)

            # mycursor.execute("INSERT INTO foodapp.DH_ORDER VALUES(%s, %s, %s);", (self.order_index, restaurant_id, student_id))
            # mydb.commit()
            # mycursor.execute("INSERT INTO foodapp.FOOD_ITEM VALUES(%s, 1.0, %s, %s)", (self.food_item_index, food_name_text, restaurant_id))
            # mydb.commit()
            # mycursor.execute("SELECT * FROM foodapp.DH_ORDER;")
            # for k in mycursor:
            #     print(k)
            # mycursor.execute("SELECT * FROM foodapp.FOOD_ITEM;")
            # for l in mycursor:
            #     print(l)

            self.order_index += 1
            self.food_item_index += 1

        self.first_name.destroy()
        self.last_name.destroy()
        self.food_name.destroy()
        #self.restaurant.destroy()

        self.first_name_label.destroy()
        self.last_name_label.destroy()
        #self.restaurant_label.destroy()
        self.food_name_label.destroy()

        self.insert_btn.destroy()

        forum_text_label = tk.StringVar()
        self.forum_label = tk.Label(master, textvariable = forum_text_label, pady = 10)
        self.forum_label.place(x = 100, y = 0)
        forum_text_label.set("Review Food Item:")

        self.forum_content = tk.Text(master, height = 10, width = 91, padx = 5, pady = 2)
        self.forum_content.place(x = 100, y = 30)

        stars_text_label = tk.StringVar()
        self.stars_label = tk.Label(master, textvariable=stars_text_label, pady = 10)
        self.stars_label.place(x = 100, y = 200)
        stars_text_label.set("Stars (1-5):")

        self.stars_content = tk.Text(master, height = 1, width = 10, padx = 5, pady = 2)
        self.stars_content.place(x = 100, y = 230)

        ins_text = tk.StringVar()
        self.insert_btn = tk.Button(master, textvariable=ins_text, height=3, width=30,
                                    command=lambda: self.insert_forum(self.master, student_id, restaurant_id))
        self.insert_btn.place(x=350, y=250)
        ins_text.set("Insert Forum")

    def insert_forum(self, master, student_id, food_id):
        stars = self.stars_content.get("1.0", 'end-1c')
        content = self.forum_content.get("1.0", 'end-1c')
        mycursor.execute("INSERT INTO foodapp.FORUM VALUES(%s, %s, %s, %s, %s);", (self.forum_index, student_id, food_id, content, stars))
        mydb.commit()






    def clear_window(self, clicked, master):
        self.create_btn.destroy()
        self.read_btn.destroy()
        #self.update_btn.destroy()
        if self.usertype == "admin":
            self.delete_btn.destroy()

        self.function = clicked

        if clicked == "create":
            self.create_clicked = True
        elif clicked == "read":
            self.read_clicked = True
        elif clicked == "update":
            self.update_clicked = True
        elif clicked == "delete":
            self.delete_clicked = True

        food_text = tk.StringVar()
        self.food_btn = tk.Button(master, textvariable=food_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("food", self.master))
        self.food_btn.place(x=0, y=0)
        food_text.set("Food")

        dining_hall_text = tk.StringVar()
        self.dining_hall_btn = tk.Button(master, textvariable=dining_hall_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("hall", self.master))
        self.dining_hall_btn.place(x=int(self.window_width / 4) + 68, y=0)
        dining_hall_text.set("Dining Hall")

        # dining_plan_text = tk.StringVar()
        # self.dining_plan_btn = tk.Button(master, textvariable=dining_plan_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:clear_window_1(self, "plan", master))
        # self.dining_plan_btn.place(x=2 * (int(self.window_width / 4)), y = 0)
        # dining_plan_text.set("Dining Plan")

        employee_text = tk.StringVar()
        self.employee_btn = tk.Button(master, textvariable=employee_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("employee", self.master))
        self.employee_btn.place(x=3 * (int(self.window_width / 4) - 42), y = 0)
        employee_text.set("Employee")

        all_item_text = tk.StringVar()
        self.all_item_btn = tk.Button(master, textvariable=all_item_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("food", self.master))
        self.all_item_btn.place(x = 0, y = int(self.window_height / 2))
        all_item_text.set("All")

        # forum_text = tk.StringVar()
        # self.forum_btn = tk.Button(master, textvariable=forum_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:clear_window_1(self, "forum", master))
        # self.forum_btn.place(x=int(self.window_width / 4), y= 1 * int(self.window_height / 2))
        # forum_text.set("Forum")

        restaurant_text = tk.StringVar()
        self.restaurant_btn = tk.Button(master, textvariable=restaurant_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("restaurant", self.master))
        self.restaurant_btn.place(x=2 * int(self.window_width / 4) - 181, y= 1 * int(self.window_height / 2))
        restaurant_text.set("Restaurant")

        student_text = tk.StringVar()
        self.student_btn = tk.Button(master, textvariable=student_text, height=self.button_height, width=int(self.button_width / 2), command = lambda:self.clear_window_1("student", self.master))
        self.student_btn.place(x=3 * int(self.window_width / 4) - 120, y= 1 * int(self.window_height / 2))
        student_text.set("Student")

    def clear_window_admin(self, master):
        self.create_btn.destroy()
        self.view_btn.destroy()
        self.read_btn.destroy()
        self.delete_btn.destroy()
        food_text = tk.StringVar()
        self.food_btn = tk.Button(master, textvariable=food_text, height=self.button_height,
                                  width=int(self.button_width / 2), command=lambda: self.delete_entity(master, "food"))
        self.food_btn.place(x=0, y=0)
        food_text.set("Food")

        dining_hall_text = tk.StringVar()
        self.dining_hall_btn = tk.Button(master, textvariable=dining_hall_text, height=self.button_height,
                                         width=int(self.button_width / 2),
                                         command=lambda: self.delete_entity(master, "hall"))
        self.dining_hall_btn.place(x=int(self.window_width / 4) + 68, y=0)
        dining_hall_text.set("Dining Hall")

        restaurant_text = tk.StringVar()
        self.restaurant_btn = tk.Button(master, textvariable=restaurant_text, height=self.button_height,
                                        width=int(self.button_width / 2),
                                        command=lambda: self.delete_entity(master, "restaurant"))
        self.restaurant_btn.place(x=2 * int(self.window_width / 4) - 181, y=1 * int(self.window_height / 2))
        restaurant_text.set("Restaurant")


    def clear_window_1(self, clicked, master):
        if clicked == "food":
            self.food_clicked = True
        elif clicked == "hall":
            self.dining_hall_clicked = True
        # elif clicked == "plan":
        #     self.dining_hall_clicked = True
        elif clicked == "employee":
            self.employee_clicked = True
        elif clicked == "food":
            self.food_item_clicked = True
        # elif clicked == "forum":
        #     self.forum_clicked = True
        elif clicked == "restaurant":
            self.restaurant_clicked = True
        elif clicked == "student":
            self.student_clicked = True

        self.entity = clicked

        self.food_btn.destroy()
        self.dining_hall_btn.destroy()
        # self.dining_plan_btn.destroy()
        self.employee_btn.destroy()
        self.food_item_btn.destroy()
        # self.forum_btn.destroy()
        # self.forum_btn.destroy()
        self.restaurant_btn.destroy()
        self.student_btn.destroy()

        if self.function == "create":
            self.create_entity(self, master)

        self.password_checkbutton.destroy()

        self.function = clicked

        if clicked == "create":
            self.create_clicked = True
        elif clicked == "read":
            self.read_clicked = True
        elif clicked == "update":
            self.update_clicked = True
        elif clicked == "delete":
            self.delete_clicked = True

        food_text = tk.StringVar()
        self.food_btn = tk.Button(master, textvariable=food_text, height=self.button_height, width=int(self.button_width / 2), command = lambda: self.clear_window_1("food", master))
        self.food_btn.place(x=0, y=0)
        food_text.set("Food")

        dining_hall_text = tk.StringVar()
        self.dining_hall_btn = tk.Button(master, textvariable=dining_hall_text, height=self.button_height, width=int(self.button_width / 2), command = lambda: self.clear_window_1("hall", master))
        self.dining_hall_btn.place(x=int(self.window_width / 4) + 68, y=0)
        dining_hall_text.set("Dining Hall")

        restaurant_text = tk.StringVar()
        self.restaurant_btn = tk.Button(master, textvariable=restaurant_text, height=self.button_height, width=int(self.button_width / 2), command = lambda: self.clear_window_1("restaurant", master))
        self.restaurant_btn.place(x=2 * int(self.window_width / 4) - 181, y= 1 * int(self.window_height / 2))
        restaurant_text.set("Restaurant")

    def clear_window_1(self, clicked, master):
        if clicked == "food":
            self.food_clicked = True
        elif clicked == "hall":
            self.dining_hall_clicked = True
        # elif clicked == "plan":
        #     self.dining_hall_clicked = True
        elif clicked == "employee":
            self.employee_clicked = True
        elif clicked == "food":
            self.food_item_clicked = True
        # elif clicked == "forum":
        #     self.forum_clicked = True
        elif clicked == "restaurant":
            self.restaurant_clicked = True
        elif clicked == "student":
            self.student_clicked = True

        self.entity = clicked

        self.food_btn.destroy()
        self.dining_hall_btn.destroy()
        self.restaurant_btn.destroy()

        print(self.function)

        if self.function == "create":
            self.create_entity(master)
        if self.function == "delete":
            self.delete_entity(master)

    def delete_entity(self, master, passed):
        self.food_btn.destroy()
        self.dining_hall_btn.destroy()
        self.restaurant_btn.destroy()
        if passed == "food":
            self.food_name_text_label = tk.StringVar()
            self.food_name_label = tk.Label(master, textvariable=self.food_name_text_label, pady=10)
            self.food_name_label.place(x=100, y=60)
            self.food_name_text_label.set("Name of Food:")

            self.food_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.food_name.place(x=100, y=90)

            self.restaurant_text_label = tk.StringVar()
            self.restaurant_label = tk.Label(master, textvariable=self.restaurant_text_label, pady=7)
            self.restaurant_label.place(x=100, y=115)
            self.restaurant_text_label.set("Name of Restaurant:")

            self.restaurant = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.restaurant.place(x=100, y=150)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                              command=lambda: self.admin_delete(master, "food"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Delete")
        elif passed == "hall":
            hall_label_text = tk.StringVar()
            self.hall_label = tk.Label(master, textvariable=hall_label_text, pady=10)
            self.hall_label.place(x=100, y=60)
            hall_label_text.set("Dining Hall Name: ")

            self.hall_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.hall_name.place(x=100, y=90)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                              command=lambda: self.admin_delete(master, "hall"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Delete")
        elif passed == "restaurant":
            restaurant_name_label_text = tk.StringVar()
            self.restaurant_name_label = tk.Label(master, textvariable=restaurant_name_label_text, pady=10)
            self.restaurant_name_label.place(x=100, y=60)
            restaurant_name_label_text.set("Name of Restaurant: ")

            self.restaurant_name = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.restaurant_name.place(x=100, y=90)

            res_dining_hall_label_text = tk.StringVar()
            self.res_dining_hall_label = tk.Label(master, textvariable=res_dining_hall_label_text, pady=10)
            self.res_dining_hall_label.place(x=100, y=180)
            res_dining_hall_label_text.set("Residing Dining Hall: ")

            self.d_hall = tk.Text(master, height=0, width=91, padx=5, pady=2)
            self.d_hall.place(x=100, y=210)

            admin_insert_text = tk.StringVar()
            self.admin_insert_btn = tk.Button(master, textvariable=admin_insert_text, height=3, width=30,
                                              command=lambda: self.admin_delete(master, "restaurant"))
            self.admin_insert_btn.place(x=350, y=300)
            admin_insert_text.set("Delete")

    def admin_delete(self, master, entity):
        if self.entity == "hall":
            name_val = self.hall_name.get("1.0", 'end-1c')

            if self.execute_sql:
                mycursor.execute("DELETE FROM foodapp.DINING_HALL WHERE DH_Name = %s;", (name_val,))
                mydb.commit()
        elif self.entity == "restaurant":
            restaurant_val = self.restaurant_name.get("1.0", 'end-1c')
            d_hall_val = self.d_hall.get("1.0", 'end-1c')
            d_hall_id = 0

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.DINING_HALL WHERE DH_Name = %s;", (d_hall_val,))
                for i in mycursor:
                    d_hall_id = i[0]
                mycursor.execute("DELETE FROM foodapp.RESTAURANT WHERE RestaurantName = %s AND Dining_Hall_ID = %s;", (restaurant_val, d_hall_id))
                mydb.commit()
        elif self.entity == "food":
            food_value = self.food_name.get("1.0", 'end-1c')
            restaurant_value = self.restaurant.get("1.0", 'end-1c')
            restaurant_id = 0

            if self.execute_sql:
                mycursor.execute("SELECT * FROM foodapp.RESTAURANT WHERE RestaurantName = %s;", (restaurant_value,))
                for i in mycursor:
                    restaurant_id = i[0]
                mycursor.execute("DELETE FROM foodapp.FOOD_ITEM WHERE FoodName = %s AND Restaurant_ID = %s;", (food_value, restaurant_id))
                mydb.commit()

    def create_entity(self, master):
        if self.entity == "hall":
            dining_hall_text_label = tk.StringVar()
            dining_hall_label = tk.Label(master, textvariable=dining_hall_text_label, pady=10)
            dining_hall_label.place(x = 100, y = 0)
            dining_hall_text_label.set("Dining Hall Name:")

            self.dining_hall_name = tk.Text(master, height=0, width=100, padx=15, pady=10)
            self.dining_hall_name.tag_configure("center", justify='center')
            self.dining_hall_name.place(x=100, y=40)

            hours_label_text = tk.StringVar()
            hours_label = tk.Label(master, textvariable=hours_label_text, pady = 10)
            hours_label.place(x=100, y = 90)
            hours_label_text.set("Hours:")

            self.hours_box = tk.Text(master, height=0, width=100, padx = 15, pady = 10)
            self.hours_box.place(x = 100, y = 130)

            location_label_text = tk.StringVar()
            location_label = tk.Label(master, textvariable=location_label_text, pady = 10)
            location_label.place(x=100, y = 170)
            location_label_text.set("Location:")

            self.location_box = tk.Text(master, height=0, width=100, padx = 15, pady = 10)
            self.location_box.place(x=100, y=210)

            insert_text = tk.StringVar()
            insert_btn = tk.Button(master, textvariable=insert_text, height=1, width=10, command = lambda:self.insert_dining_hall(self.master))
            insert_btn.place(x=475, y = 270)
            insert_text.set("Insert")

        elif self.entity == "plan":
            plan_name_label_text = tk.StringVar()
            plan_name_label = tk.Label(master, textvariable=plan_name_label_text, pady=10)
            plan_name_label.place(x=100, y=10)
            plan_name_label_text.set("Plan Name:")

            plan_name_box = tk.Text(master, height = 0, width = 100, padx=15, pady = 10)
            plan_name_box.place(x=100, y=50)

            cost_label_text = tk.StringVar()
            cost_label = tk.Label(master, textvariable=cost_label_text, pady=10)
            cost_label.place(x=100, y=80)

    def insert_dining_hall(self, master):
        d_name = self.dining_hall_name.get("1.0", 'end-1c')
        hours_name = self.hours_box.get("1.0", 'end-1c')
        location_name = self.location_box.get("1.0", 'end-1c')
        mycursor.execute("INSERT INTO foodapp.DINING_HALL VALUES(22, %s, 1, %s, %s);", (d_name, hours_name, location_name))
        mydb.commit()
        mycursor.execute("SELECT * FROM foodapp.DINING_HALL;")
        for i in mycursor:
            print(i)





if __name__ == '__main__':
    mydb = mysql.connector.connect(host="localhost", user="root", passwd="password", database="foodapp")
    mycursor = mydb.cursor()
    # mycursor.execute("SELECT * FROM foodapp.DH_ORDER;")
    # for i in mycursor:
    #     print(i)

    root = tk.Tk()
    new_window = window(root)
    root.mainloop()

